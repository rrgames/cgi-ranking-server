/*
 * Model.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef MODEL_H_
#define MODEL_H_

#include <map>
#include <vector>
#include <string>
#include "Database/MySQLPool.h"
#include "ModelField.h"
#include "FieldSet.h"


class Model {
public:
    virtual const std::string GetTableName();
    virtual const std::string GetModelName();

    Model();
    ~Model();
    Model* Select(std::string select);
    Model* New();
    Model* Delete();
	Model* Update();
    Model* AddValue(std::string columnName, int value);
    Model* AddValue(std::string columnName, std::string value);
    Model* Where(std::string where);
    Model* Limit(int count);
    std::string* GetString(std::string columnName);
    std::string* GetString(std::string columnName, int row);
    long long int* GetInt(std::string columnName);
    long long int* GetInt(std::string columnName, int row);
    int GetRowsCount();
    Model* Commit();
    FieldSet fieldset;
    FieldSet* GetFieldset();
    enum SQLCommitType { INSERT, SELECT, DELETE, UPDATE, EMPTY };

private:
    void ExecuteNonQuery(std::string query);
    void ExecuteQuery(std::string query);

    int limitCount;
    std::string selectField;
    std::string wherePassage;
    SQLCommitType commitType;
    std::map<std::string, std::string> insertMap;
};

#endif /* MODEL_H_ */
