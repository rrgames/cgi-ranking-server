#include <stdio.h>
#include <string.h>
#include "Models/FriendDAO.h"
#include "Models/FriendModel.h"
#include "Database/MongoDBPool.h"

int main() {


    std::string user_id = "1";

    FriendDAO * fDAO = new FriendDAO();
    std::vector<FriendModel *> fm = fDAO->findByUserID(user_id);

    std::string where = "id in (";
    for(int i=0; i<fm.size(); i++) {
        where += fm[i]->getFriendID();
        where += ",";
    }
    where.erase(where.size()-1);
    where+= ")";

    printf("%s\n", where.c_str());

    return 0;
}
