/*
 * Response.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "Response.h"
#include <fcgi_stdio.h>
#include <cstdlib>

std::string Response::GetResponse() {
    return header + "\r\n" + body;
}

void Response::AddHeader(std::string _header) {
    this->header += _header + "\r\n";
}

void Response::SetData(std::string jsonData) {
    this->body = jsonData;
}

void Response::SetStatus(int code) {
    status = "500 Internal Server Error";

    switch(code) {
    case 200:
        status = "200 OK";
        break;
    case 201:
        status = "201 Created";
        break;
    case 202:
        status = "202 Accepted";
        break;
    case 204:
        status = "204 No Content";
        break;
    case 301:
        status = "301 Moved Permanently";
        break;
    case 303:
        status = "303 See Other";
        break;
    case 304:
        status = "304 Not Modified";
        break;
    case 307:
        status = "307 Temporary Redirect";
        break;
    case 400:
        status = "400 Bad Request";
        break;
    case 401:
        status = "401 Unauthorized";
        break;
    case 403:
        status = "403 Forbidden";
        break;
    case 404:
        status = "404 Not Found";
        break;
    case 405:
        status = "405 Method Not Allowed";
        break;
    case 406:
        status = "406 Not Acceptable";
        break;
    case 409:
        status = "409 Conflict";
        break;
    case 412:
        status = "412 Precondition Failed";
        break;
    case 415:
        status = "415 Unsupported Media Type";
        break;
    case 500:
        status = "500 Internal Server Error";
        break;
    }

    this->AddHeader("Status: " + status);

}
void Response::InitHeader(int code) {
    //this->setStatus(code);
    this->AddHeader("Content-type: application/json; charset=utf-8");
    //this->addHeader("Server: lighttpd-cgi/1.0");
    //this->addHeader("Connection: close");
    //this->addHeader("Date: "); //
    this->SetStatus(code);
}
void Response::Render() {
	//아래는 디버깅을 위한 것임.
//    printf("%s\r\n %s\r\n %s\r\n %s\r\n", this->GetResponse().c_str(), std::getenv("REQUEST_METHOD"), std::getenv("REQUEST_URI"), std::getenv("QUERY_STRING"));
	printf("%s", this->GetResponse().c_str());
}

Response* Response::CreateResponse(int code, std::string jsonData) {
    Response* res = new Response();
    res->InitHeader(code);
    res->SetData(jsonData);
    return res;
}
void Response::RemoveResponse(Response* pointer) {
    delete pointer;
}

