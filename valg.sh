#!/bin/sh

sudo service lighttpd stop
make debug

sudo cp cgi_config/10-cgi-fcg.conf /etc/lighttpd/conf-enabled/10-cgi.conf
sudo cp test.fcgi /var/www/cgi/

cd /var/www/cgi

sudo spawn-fcgi -f "valgrind --leak-check=full --log-file="testval.log" ./test.fcgi" -p 9000

sudo service lighttpd start
