/*
 * JsonString.h
 *
 *  Created on: 2014. 9. 22.
 *      Author: into_arena
 */

#ifndef JSONSTRING_H_
#define JSONSTRING_H_

#include "JsonObject.h"
#include <string>

class JsonString : public JsonObject {
public:
    std::string ToString();
};




#endif /* JSONSTRING_H_ */
