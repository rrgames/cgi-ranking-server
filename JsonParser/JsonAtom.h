//
//  JsonAtom.h
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 23..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#ifndef __JsonBuilder__JsonAtom__
#define __JsonBuilder__JsonAtom__

#include "JsonObject.h"
#include <string>

class JsonAtom : public JsonObject {
public:
    JsonAtom() {};
    JsonAtom(std::string);
    ~JsonAtom() {};
    std::string atom;
    virtual std::string ToString();

};

// 사실 이건 boolean이라던가 이런것에 다 통용시킬거야.
class JsonNumber : public JsonAtom {
public:
    JsonNumber() {};
	JsonNumber(int numb);
    JsonNumber(long long int numb);

    std::string ToString();
};

class JsonBoolean : public JsonAtom {
public:
    JsonBoolean() {};
    JsonBoolean(bool bl);
    std::string ToString();
};

#endif /* defined(__JsonBuilder__JsonAtom__) */
