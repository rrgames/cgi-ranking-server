//
//  JsonParser.h
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#ifndef JsonBuilder_JsonParser_h
#define JsonBuilder_JsonParser_h

#include <stdlib.h>
#include <string>
#include <map>

#include "JsonObject.h"
#include "JsonArray.h"
#include "JsonAtom.h"

#include "StringFormat.h"
#include "../Model.h"

class JsonParser {
public:
    static std::string ModelToJson(std::string modelName, Model* model);
};



#endif
