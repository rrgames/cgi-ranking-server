#include "XMLStageParser.h"
#include <map>

XMLStageParser::XMLStageParser(std::string stage_name) {
    doc = xmlReadFile(stage_name.c_str(), NULL, 0);
    if(doc!= NULL) {
        root_elem = xmlDocGetRootElement(doc);
        parseXMLRecur(root_elem);
        xmlFreeDoc(doc);
    }
    xmlCleanupParser();
}

std::string XMLStageParser::ParseStageXML(std::string stage_name) {
    return nodes[stage_name];
}
// 이름을 쓰면 꺼내오는 식으로 바꾸자.
void XMLStageParser::parseXMLRecur(xmlNode * node) {
    xmlNode * cur_node = NULL;
    xmlChar * key;
    for(cur_node = node; cur_node; cur_node = cur_node->next) {
        if(cur_node->type == XML_ELEMENT_NODE) {
            key = xmlNodeGetContent(cur_node);
            nodes[(char *)cur_node->name] = (char *)key;
            xmlFree(key);
        }
        parseXMLRecur(cur_node->children);
    }
}
