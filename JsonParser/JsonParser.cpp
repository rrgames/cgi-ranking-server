//
//  JsonParser.cpp
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#include "JsonParser.h"
#include "../FieldSet.h"
#include "../StringHelper.h"
#include "JsonAtom.h"

std::string JsonParser::ModelToJson(std::string modelName, Model* model) {
	std::string result;
  
    FieldSet* fields = model->GetFieldset();

	result += "{\"" + modelName + "\":";

	if (fields->GetRowsCount() > 1) {
		
		result += "[";
		for(int y = 0; y < fields->GetRowsCount(); y++) {
			if (y != 0) {
				result += ", ";
			}
			result += "{";
			for(int x = 0; x < fields->GetColumnsCount(); x++) {
				if (x != 0) {
					result += ", ";
				}
				result += "\"" + fields->scheme[x]->fieldName + "\" : ";
                		if (fields->scheme[x]->typeName == "INT" || fields->scheme[x]->typeName == "BIGINT")
                    			result += StringHelper::IntToString((long long int)*fields->GetInt(x, y));
                		else if (fields->scheme[x]->typeName == "VARCHAR"|| fields->scheme[x]->typeName =="DATETIME" || fields->scheme[x]->typeName == "BLOB" || fields->scheme[x]->typeName == "TEXT")
                    			result += "\"" + *fields->GetString(x, y) + "\"";
			}
			result += "}";
		}
		result += "]";
	} else if (fields->GetRowsCount() == 1) {
		result += "{";
		
		for(int x = 0; x < fields->GetColumnsCount(); x++) {
			if (x != 0) {
				result += ", ";
			}
			result += "\"" + fields->scheme[x]->fieldName + "\" : ";
                	if (fields->scheme[x]->typeName == "INT" || fields->scheme[x]->typeName == "BIGINT")
                    		result += StringHelper::IntToString((long long int)*fields->GetInt(x, 0));
                	else if (fields->scheme[x]->typeName == "VARCHAR"|| fields->scheme[x]->typeName =="DATETIME" || fields->scheme[x]->typeName == "BLOB" || fields->scheme[x]->typeName == "TEXT")
                    		result += "\"" + *fields->GetString(x, 0) + "\"";
		}
		result += "}";		
	
    } else {
        result += "[]";
    }
	result += "}";
    return result;
}
