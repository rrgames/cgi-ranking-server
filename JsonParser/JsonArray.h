//
//  JsonArray.h
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#ifndef __JsonBuilder__JsonArray__
#define __JsonBuilder__JsonArray__

#include "JsonObject.h"
#include <string>
#include <vector>

// 일단 이 요소는 다음에 생각하자.
class JsonArray : public JsonObject {
public:
    JsonArray() { };
    virtual ~JsonArray();
    std::string ToString();

    void Append(JsonObject * obj);
    void Append(int number);
	void Append(long long int number);
    void Append(bool bl);
    void Append(std::string str);
private:
    std::vector<JsonObject*> array;

};

#endif /* defined(__JsonBuilder__JsonArray__) */
