//
//  JsonObject.h
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#ifndef JsonBuilder_JsonObject_h
#define JsonBuilder_JsonObject_h

#include <string>
#include <map>

class JsonObject {
public:
    JsonObject();
    virtual ~JsonObject();

    std::map<std::string, JsonObject*> contents;

    virtual std::string ToString();
    void AppendObject(std::string str, JsonObject* obj);
    void AppendObject(std::string str, std::string atom);
    void AppendObject(std::string str, int atom);
    void AppendObject(std::string str, long long int atom);
    void AppendObject(std::string str, bool bl);
};

#endif
