//
//  JsonObject.cpp
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 22..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#include "JsonObject.h"
#include "JsonAtom.h"
#include "StringFormat.h"

typedef std::pair <std::string, JsonObject * > obj_pair;

JsonObject::JsonObject() {

}

JsonObject::~JsonObject() {
    std::map<std::string, JsonObject *>::iterator iter;
    //메모리 해제를 하자.
    if(!contents.empty()) {
        for(iter = contents.begin(); iter!= contents.end(); iter++) {
            if(iter->second)
                delete iter->second;
        }
    }
}

std::string JsonObject::ToString() {
    std::map<std::string, JsonObject *>::iterator iter;

    std::string buffer = "{";

    for(iter = contents.begin(); iter != contents.end(); iter++) {
        //string format 을 정해놓는 것이 좋을듯 하다.
        buffer += "\"";
        buffer += iter->first;
        buffer += "\":";
        buffer += iter->second->ToString();
        buffer += ",";
    }

    // end에 있는 쉼표 표시 삭제.
    buffer.erase(buffer.size()-1);
    buffer += "}";
    return buffer;
}



void JsonObject::AppendObject(std::string str, std::string atom) {
    JsonAtom * objs = new JsonAtom(atom);
    contents.insert(obj_pair(str, objs));
}


void JsonObject::AppendObject(std::string str, JsonObject* obj) {
    contents.insert(obj_pair(str, obj));
}

void JsonObject::AppendObject(std::string str, int atom) {
    JsonNumber * objs = new JsonNumber(atom);
    contents.insert(obj_pair(str, objs));
}
void JsonObject::AppendObject(std::string str, long long int atom) {
    JsonNumber * objs = new JsonNumber(atom);
    contents.insert(obj_pair(str, objs));
}
void JsonObject::AppendObject(std::string str, bool bl) {
    JsonBoolean * objs = new JsonBoolean(bl);
    contents.insert(obj_pair(str, objs));
}


