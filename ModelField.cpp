/*
 * ModelField.cpp
 *
 *  Created on: 2014. 9. 21.
 *      Author: bae-unidev
 */

#include "ModelField.h"

ModelField::ModelField(std::string name, int index, std::string typeName) {
    this->fieldName = name;
    this->index = index;
    this->typeName = typeName;
}

ModelField::~ModelField() {
}
