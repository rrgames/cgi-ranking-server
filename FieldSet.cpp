/*
 * FieldSet.cpp
 *
 *  Created on: 2014. 9. 22.
 *      Author: bae-unidev
 */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "FieldSet.h"

FieldSet::FieldSet()
{
	stringTable = std::vector<std::string>();
	intTable = std::vector<long long int>();
}

int FieldSet::GetColumnsCount() {
    return columnCount;
}

int FieldSet::GetRowsCount() {
    return rowCount;
}

FieldSet::~FieldSet() {
	Clear();
   // std::map<int, ModelField*>::iterator iter;
   // for(iter = scheme.begin(); iter != scheme.end(); iter++)
    //    delete (iter->second);
}

void FieldSet::Clear() {
	intTable.clear();
	stringTable.clear();
	schemeName.clear();

	std::map<int, ModelField*>::iterator iter;
for(iter = scheme.begin(); iter != scheme.end(); iter++)
delete (iter->second);

	scheme.clear();

}
void FieldSet::SetSize(int col, int row) {
	columnCount = col;
	rowCount = row;
	intTable.resize(col * row);
	stringTable.resize(col * row);
}
void FieldSet::SetScheme(std::string name, int index, std::string typeName) {
    this->schemeName[name] = index;
    this->scheme[index] = new ModelField(name, index, typeName);
}

void FieldSet::SetData(sql::ResultSet* value) {
    int row = 0;

    while (value->next()) {
        for (int index = 0; index < this->columnCount; index++) {
            if (this->scheme[index]->typeName == "INT" || this->scheme[index]->typeName == "BIGINT")
                intTable[row * this->columnCount + index] = value->getUInt64(scheme[index]->fieldName);
            else if (this->scheme[index]->typeName == "VARCHAR" || this->scheme[index]->typeName == "DATETIME" || this->scheme[index]->typeName == "BLOB" || this->scheme[index]->typeName == "TEXT")
				if(value->getString(scheme[index]->fieldName)=="")
					stringTable[row * this->columnCount + index] = "null";
				else
                	stringTable[row * this->columnCount + index] = value->getString(scheme[index]->fieldName);
        }
        row++;
    }
}

int FieldSet::GetColumnIndex(std::string value) {
    if (this->schemeName.find(value) != this->schemeName.end()) 
   	 return this->schemeName[value];
    else
	return -1;
}

long long int* FieldSet::GetInt(int column, int row) {
    if (column < this->columnCount && row < this->rowCount)	{
        return &(intTable[row * this->columnCount + column]);
    } else {
        std::cout << "fieldTable보다 크기가 큽니다." << std::endl;
        return NULL;
    }
}

std::string* FieldSet::GetString(int column, int row) {
    if (column < this->columnCount && row < this->rowCount) {
        return &(stringTable[row * this->columnCount + column]);
    } else {
        std::cout << "fieldTable보다 크기가 큽니다." << std::endl;
        return NULL;
    }
}
