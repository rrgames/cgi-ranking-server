/*
 * MysqlPool.h
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#ifndef MONGODBPOOL_H_
#define MONGODBPOOL_H_

#include <bson.h>
#include <mongoc.h>
#include <stdio.h>
#include <string>
#include <map>

class MongoDBPool {
public:
    // 여기서 우리는 db, collection이렇게 고르게 되는데...
    // 우리의 db는 newdb_development. 콜렉션은 friends가 된다.
    // 우린 콜렉션에만 집중하게 될것 같다.
// 클라이언트를 여러개 열고, DB가 각각 하나씩 대응되는 형국이어야 한다.
    MongoDBPool(std::string url);
    ~MongoDBPool();
    mongoc_client_t* GetClientFromPool();
    void ReleaseClientToPool(mongoc_client_t* client);
    bool CreatePool(int poolSize);
    static MongoDBPool* GetInstance();
    static MongoDBPool* instance;
    enum ConnectionStatus { USE, FREE, TEMP };

private:
    std::map<mongoc_client_t*, MongoDBPool::ConnectionStatus> pool;
    int poolSize;
    int maxConnection;
    std::string clientName;
	std::string databaseName;
    //mongoc_client_t* client;
    mongoc_client_t* CreateClient();
};

#endif /* MONGODBPOOL_H_ */
