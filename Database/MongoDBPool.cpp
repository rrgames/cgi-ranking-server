/*
 * MongoDBPool.cpp
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#include "MongoDBPool.h"

MongoDBPool* MongoDBPool::instance = NULL;
MongoDBPool* MongoDBPool::GetInstance() {
    if (instance == NULL) {
        instance = new MongoDBPool("mongodb://localhost:27017/");
        instance->CreatePool(5);
    }
    return instance;
}

MongoDBPool::MongoDBPool(std::string url) {
    this->maxConnection = -1;
    this->poolSize = 0;
	this->clientName = url;
//    client = mongoc_client_new(url.c_str());
}
MongoDBPool::~MongoDBPool() {
    std::map<mongoc_client_t *, MongoDBPool::ConnectionStatus>::iterator iter;
    for (iter = pool.begin(); iter != pool.end(); iter++) {
        mongoc_client_destroy(iter->first);
    }
}

mongoc_client_t * MongoDBPool::CreateClient() {
    mongoc_client_t * client = NULL;

    //client에 database객체 삽입.
    //client = mongoc_client_get_database(this->client, this->databaseName.c_str());

	client = mongoc_client_new(this->clientName.c_str());
    return client;
}

mongoc_client_t * MongoDBPool::GetClientFromPool() {
    mongoc_client_t * client = NULL;
    std::map<mongoc_client_t *, MongoDBPool::ConnectionStatus>::iterator iter;

    for (iter = pool.begin(); iter != pool.end(); iter++) {
        if (iter->second == MongoDBPool::FREE) {
            client = iter->first;
            iter->second = MongoDBPool::USE;
            break;
        }
    }
    if (client == NULL) {
        client = this->CreateClient();
        pool.insert(std::make_pair(client, MongoDBPool::TEMP));
    }
    return client;
}

void MongoDBPool::ReleaseClientToPool(mongoc_client_t * client) {
    std::map<mongoc_client_t *, MongoDBPool::ConnectionStatus>::iterator iter = pool.find(client);
    if(iter != pool.end()) {
        if(iter->second == MongoDBPool::USE)
            iter->second = MongoDBPool::FREE;

        if(iter->second == MongoDBPool::TEMP) {
            mongoc_client_destroy(iter->first);
            pool.erase(iter);
        }
    }
}

// 아예 처음부터 워커를 5개 만들어 놓는거였다니.
bool MongoDBPool::CreatePool(int poolSize) {
    for(int i = 0; i < poolSize; i++) {
        mongoc_client_t * client = this->CreateClient();
        if (client != NULL) {
            pool.insert(std::make_pair(client, MongoDBPool::FREE));
        }
    }

    if (poolSize == pool.size()) {
        this->poolSize = poolSize;
        return true;
    }
    return false;
}
