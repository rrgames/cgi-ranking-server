/*
 * MysqlPool.h
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#ifndef MYSQLPOOL_H_
#define MYSQLPOOL_H_

#include <string>
#include <map>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>
#include <mysql_driver.h>
#include <mysql_connection.h>

class MySQLPool {

public:
    MySQLPool(std::string id, std::string password, std::string url);
    ~MySQLPool();
    sql::Connection* GetConnectionFromPool();
    void ReleaseConnectionToPool(sql::Connection* connection);
    bool CreatePool(int poolSize);
    static MySQLPool* GetInstance();
    static MySQLPool* instance;
    enum ConnectionStatus { USE, FREE, TEMP };

private:
    sql::mysql::MySQL_Driver* driver;
    std::string id;
    std::string password;
    std::string url;
    std::string databaseName;

    std::map<sql::Connection*, MySQLPool::ConnectionStatus> pool;
    int poolSize;
    int maxConnection;
    sql::Connection* CreateConnection();
};

#endif /* MYSQLPOOL_H_ */
