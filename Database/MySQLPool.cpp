/*
 * MysqlPool.cpp
 *
 *  Created on: 2014. 9. 17.
 *      Author: bae-unidev
 */

#include "MySQLPool.h"

MySQLPool* MySQLPool::instance = NULL;

MySQLPool* MySQLPool::GetInstance() {
    if (instance == NULL) {
        instance = new MySQLPool("root", "dlalsgur", "localhost:3306");
        instance->CreatePool(5);
    }
    return instance;
}

MySQLPool::MySQLPool(std::string id, std::string password, std::string url) {
    this->id = id;
    this->password = password;
    this->url = url;
    this->maxConnection = -1;
    this->poolSize = 0;
    this->databaseName = "rankserver_development";
    try {
        driver = sql::mysql::get_driver_instance();
    } catch(sql::SQLException &e) {
        std::cout << "MYSQL driver init error! " << e.getErrorCode() << " at " << __FUNCTION__ <<std::endl;
    }
}
MySQLPool::~MySQLPool() {
    std::map<sql::Connection*, MySQLPool::ConnectionStatus>::iterator iter;
    for (iter = pool.begin(); iter != pool.end(); iter++) {
        delete iter->first;
    }
    pool.clear();
}
sql::Connection* MySQLPool::CreateConnection() {
    sql::Connection* connection = NULL;
    try {
        connection = driver->connect(this->url, this->id, this->password);
        connection->setSchema(this->databaseName);
    } catch(sql::SQLException &e) {
        std::cout << "MYSQL connection error " << e.getErrorCode() << " at " << __FUNCTION__ <<std::endl;
    }
    return connection;
}
sql::Connection* MySQLPool::GetConnectionFromPool() {
    sql::Connection* connection = NULL;
    std::map<sql::Connection*, MySQLPool::ConnectionStatus>::iterator iter;
    for (iter = pool.begin(); iter != pool.end(); iter++) {
        if (iter->second == MySQLPool::FREE) {
            connection = iter->first;
            iter->second = MySQLPool::USE;
            break;
        }
    }
    if (connection == NULL) {
        connection = this->CreateConnection();
        pool.insert(std::make_pair(connection, MySQLPool::TEMP));
    }
    return connection;
}
void MySQLPool::ReleaseConnectionToPool(sql::Connection* connection) {
    std::map<sql::Connection*, MySQLPool::ConnectionStatus>::iterator iter = pool.find(connection);
    if(iter != pool.end()) {
        if(iter->second == MySQLPool::USE)
            iter->second = MySQLPool::FREE;

        if(iter->second == MySQLPool::TEMP) {
            delete iter->first;
            pool.erase(iter);
        }
    }
}

bool MySQLPool::CreatePool(int poolSize) {
    for(int i = 0; i < poolSize; i++) {
        sql::Connection* connection = this->CreateConnection();
        if (connection != NULL) {
            pool.insert(std::make_pair(connection, MySQLPool::FREE));
        }
    }

    if (poolSize == pool.size()) {
        this->poolSize = poolSize;
        return true;
    }
    return false;
}
