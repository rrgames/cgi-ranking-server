#ifndef _FBGRAPH_H_
#define _FBGRAPH_H_

#include <json-c/json.h>
#include <curl/curl.h>
#include <map>
#include <string>
#include <stdlib.h>
#include <string.h>
#include <vector>


class FBApp {
public:
	static std::string GetAccessToken(const std::string &code);
private:
    static const std::string fb_app_id;
    static const std::string fb_app_secret;
    static const std::string fb_redirect_uri;
    static const std::string fb_oauth_url;
};

class FBGraph {
public:
    FBGraph();
    FBGraph(std::string _access_token);
    ~FBGraph();
    void SetMyInfo();
    static const std::string fb_graph_url;
    int jsonInfoParse(json_object *jobj);

    std::string GetProfilePhotoPath();
    std::string GetMyElement(const std::string &str);
    std::vector<std::string> GetFriendList();
	static int ShareResult(const std::string &_access_token, const std::string &message);
private:
    std::string access_token;
    std::map<std::string, std::string> my_info;

};
#endif
