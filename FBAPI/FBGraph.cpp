#include "FBGraph.h"
#include "../StringHelper.h"
#include "../RestClient.h"
const std::string FBApp::fb_app_id = "1512783212267771";
const std::string FBApp::fb_app_secret = "3e6ac3f6ef903425006fd02a4aa3fd86";
const std::string FBApp::fb_redirect_uri = "https://rrgames.me:3000/client/";
const std::string FBApp::fb_oauth_url = "https://graph.facebook.com/oauth";

std::string FBApp::GetAccessToken(const std::string &code) {
	RestClient::clearAuth();

    std::string query_string = fb_oauth_url +
                               "/access_token?client_id=" + fb_app_id +
                               "&client_secret=" + fb_app_secret +
                               "&redirect_uri=" + fb_redirect_uri +
                               "&code=" + code;

	RestClient::response resp = RestClient::get(query_string);

	if(resp.code > 300 || resp.code < 199){
		return "";
	}
    std::string access_token = resp.body;
    access_token = StringHelper::SplitByToken("&", access_token)[0];
	int e_loc = access_token.find("=");
	std::string real_tok = access_token.substr(e_loc+1);
    return real_tok; 
}

const std::string FBGraph::fb_graph_url = "https://graph.facebook.com/v2.1/";

FBGraph::FBGraph() {
}

FBGraph::FBGraph(std::string _access_token) {
    access_token = _access_token;
}

std::string FBGraph::GetMyElement(const std::string &str) {
    return my_info[str];
}

int FBGraph::jsonInfoParse(json_object *jobj) {

    enum json_type type;
    json_object_object_foreach(jobj, key, val) {

        type = json_object_get_type(val);
        switch(type) {
        case json_type_string:
            my_info[key] = json_object_get_string(val);
        }

    }
    return 0;
}

std::string FBGraph::GetProfilePhotoPath() {
	RestClient::clearAuth();
    std::string query_string =
        fb_graph_url + "me/picture?redirect=false&access_token=" +
        access_token;
    //parse json and return path

	RestClient::response resp = RestClient::get(query_string);
	if(resp.code > 300 || resp.code < 199){
		return "";
	}
    std::string picturePath = "";
    json_object *jobj = json_tokener_parse(resp.body.c_str());
    json_object *child;
    if(json_object_object_get_ex(jobj, "data", &child)) {
        if(json_object_object_get_ex(child, "url", &child)) {
            picturePath= json_object_get_string(child);
        }
    }
    return picturePath;
}

std::vector<std::string> FBGraph::GetFriendList() {
    // 친구 리스트를 끌어오자.
    std::vector<std::string> result;
    std::string query_string =
        fb_graph_url + "me/friends?access_token=" +
        access_token;
	RestClient::clearAuth();
	RestClient::response resp = RestClient::get(query_string);

    json_object *jobj = json_tokener_parse(resp.body.c_str());
    json_object *child;
    json_object *jvalue;
    int json_length;
    std::string x;
    if(json_object_object_get_ex(jobj, "data", &child)) {
        json_length = json_object_array_length(child);
        for(int i = 0; i<json_length; i++) {
            jvalue = json_object_array_get_idx(child, i);
            if(json_object_object_get_ex(jvalue, "id", &jvalue)) {
                result.push_back(json_object_get_string(jvalue));
            }
        }
    }
    return result;
}

void FBGraph::SetMyInfo() {

    std::string query_string =
        fb_graph_url + "me?access_token=" +
        access_token;

	RestClient::clearAuth();
	RestClient::response resp = RestClient::get(query_string);

	if(resp.code > 300 || resp.code <199){
		return;
	}
    /*
       Parse Json data Using Fetched DATA
     */
    json_object *jobj = json_tokener_parse(resp.body.c_str());
    jsonInfoParse(jobj);
}

FBGraph::~FBGraph() {
}

int FBGraph::ShareResult(const std::string &_access_token,const std::string &message){
	std::string query_string = fb_graph_url + "me/feed?access_token=" +
		_access_token;

	std::string data = "message=" + message;
	RestClient::clearAuth();
	RestClient::response resp = RestClient::post(query_string, "application/x-www-form-urlencoded", data);

	if(resp.code > 300 || resp.code < 199){
		return -1;
	}
// 결과 처리 한다.
}
