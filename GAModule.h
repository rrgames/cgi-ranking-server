#ifndef _GAMODULE_H_
#define _GAMODULE_H_

#include <curl/curl.h>
#include <vector>
#include <string>

class GAModule{
public:
	GAModule();
	~GAModule();
	void AppendFormData(std::string key, std::string value);
	void Commit(std::string url, std::string uuid);
private:
	static const char * GAURL;
	static const char * GAID;
	std::vector<std::string> formData;
protected:
};

#endif
