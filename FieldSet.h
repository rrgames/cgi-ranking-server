/*
  * FieldSet.h
 *
 *  Created on: 2014. 9. 22.
 *      Author: bae-unidev
 */

#ifndef FIELDSET_H_
#define FIELDSET_H_

#include <vector>
#include <string>
#include <map>
#include "Database/MySQLPool.h"
#include "ModelField.h"

class FieldSet {
public:
    FieldSet();
    ~FieldSet();

    void SetData(sql::ResultSet* value);
    void SetScheme(std::string name, int index, std::string typeName);
    void Clear();
    void SetSize(int col, int row);

    int GetColumnIndex(std::string columnName);
    int GetColumnsCount();
    int GetRowsCount();
    long long int* GetInt(int column, int row);
    std::string* GetString(int column, int row);
    std::map<std::string, int> schemeName;
    std::map<int, ModelField*> scheme;

private:
    int columnCount;
    int rowCount;
    std::vector<long long int> intTable;
    std::vector<std::string> stringTable;
};

#endif /* FIELDSET_H_ */
