#include "GAModule.h"
#include "LogModule.h"

const char * GAModule::GAURL = "http://www.google-analytics.com/collect";
const char * GAModule::GAID = "tid=UA-55619482-1";

void GAModule::AppendFormData(std::string key, std::string value) {
	std::string form_data = key + "=" + value;
	formData.push_back(form_data);
}
GAModule::GAModule() {
}
GAModule::~GAModule() {
}

void GAModule::Commit(std::string url, std::string uuid) {
	CURL * curl;
	CURLcode res;

	curl = curl_easy_init();
	curl_easy_setopt(curl, CURLOPT_URL, GAURL);

	std::string f_data= "v=1&" + (std::string)GAID;

	for(int i = 0; i< formData.size(); i++){
		f_data += "&";
		f_data += formData[i];
	}
	f_data += "&t=pageview";
	f_data += "&cid=" + uuid;
	f_data += "&dp=" + url;
	LogModule::GetInstance()->Debug("",f_data); 
	curl_easy_setopt(curl, CURLOPT_POSTFIELDS, f_data.c_str());
	curl_easy_setopt(curl, CURLOPT_POST, 1);
	res=curl_easy_perform(curl);
	if(res != CURLE_OK){
		LogModule::GetInstance()->Debug("", curl_easy_strerror(res));
	}
	curl_easy_cleanup(curl);
}
