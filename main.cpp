#include <fcgi_stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>

#include "Environment.h"
#include "Routes.h"
#include "FieldSet.h"
#include "LogModule.h"

#include "Database/REDISPool.h"
#include "Database/MongoDBPool.h"
#include "Database/MySQLPool.h"

#include "Models/SessionContainer.h"

int main(void) {
    // 싱글톤 인스턴스 생성.
    MySQLPool* databasePool = MySQLPool::GetInstance();
    MongoDBPool* mongoPool = MongoDBPool::GetInstance();
    RedisPool* redisPool = RedisPool::GetInstance();
    LogModule* logModule = LogModule::GetInstance();
    SessionContainer* sessionContainer = SessionContainer::GetInstance();

    while(FCGI_Accept() >= 0) {
        Routes routes;
        routes.RouteUrl();
    }

    delete databasePool;
    delete redisPool;
    delete mongoPool;
    delete logModule;
    delete sessionContainer;

    return 0;
}
