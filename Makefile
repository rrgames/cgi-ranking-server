CC = gcc
TARGET = test
CXX = g++
SHELL = /bin/sh
ECHO  = /bin/echo
GLIB := $(shell pkg-config --libs libmongoc-1.0)
LIBBSON = /usr/local/include/libbson-1.0/
LIBMONGO = /usr/local/include/libmongoc-1.0/
LIBXML2 = /usr/include/libxml2/

SOURCES = main.cpp \
		  Controllers/UsersController.cpp \
		  Database/MySQLPool.cpp \
		  Controller.cpp \
		  Environment.cpp \
		  Routes.cpp \
		  ModelField.cpp \
		  FieldSet.cpp \
		  Response.cpp \
		  Request.cpp \
		  StringHelper.cpp \
		  Url.cpp


JsonParser/XMLStageParser.o : JsonParser/XMLStageParser.cpp
	$(CXX) -g -c -o $@ JsonParser/XMLStageParser.cpp -I$(LIBXML2) -lxml2 

Database/MongoDBPool.o: Database/MongoDBPool.cpp
	$(CXX) -g -c -o $@ Database/MongoDBPool.cpp -I$(LIBBSON) -I$(LIBMONGO)

Database/REDISPool.o : Database/REDISPool.cpp
	$(CXX) -g -c -o $@ Database/REDISPool.cpp $(shell pkg-config --cflags --libs redis3m)

Models/FriendDAO.o : Models/FriendDAO.cpp
	$(CXX) -g -c -o $@ Models/FriendDAO.cpp -I$(LIBBSON) -I$(LIBMONGO)

Models/FriendModel.o : Models/FriendModel.cpp
	$(CXX) -g -c -o $@ Models/FriendModel.cpp -I$(LIBBSON) -I$(LIBMONGO)

Controllers/FriendsController.o : Controllers/FriendsController.cpp
	$(CXX) -D DEBUG -g -c -o $@ Controllers/FriendsController.cpp -I$(LIBBSON) -I$(LIBMONGO)

Controllers/StagesController.o : Controllers/StagesController.cpp
	$(CXX) -g -c -o $@ Controllers/StagesController.cpp -I$(LIBXML2)

Controllers/SessionController.o : Controllers/SessionController.cpp
	$(CXX) -D DEBUG -g -c -o $@ Controllers/SessionController.cpp -I$(LIBBSON) -I$(LIBMONGO)

Controllers/RecordController.o : Controllers/RecordController.cpp
	$(CXX) -g -c -o $@ Controllers/RecordController.cpp -I$(LIBBSON) -I$(LIBMONGO)

Model.o : Model.cpp 
	$(CXX) -g -c -o $@ Model.cpp

Models/UserModel.o : Models/UserModel.cpp
	$(CXX) -g -c -o $@ Models/UserModel.cpp

Controllers/UsersController.o : Controllers/UsersController.cpp
	$(CXX) -g -c -o $@ Controllers/UsersController.cpp

FBAPI/FBGraph.o : FBAPI/FBGraph.cpp
	$(CXX) -g -c -o $@ FBAPI/FBGraph.cpp -lcurl -ljson-c

main.o : main.cpp
	$(CXX) -g -c -o $@ main.cpp -I$(LIBBSON) -I$(LIBMONGO)

LIBS = main.o \
	   LogModule.o \
	   GAModule.o \
	   RestClient.o \
	   Database/MySQLPool.o \
	   Database/MongoDBPool.o \
	   Database/REDISPool.o \
	   Models/FriendModel.o \
	   Models/FriendDAO.o \
	   Models/StageModel.o \
	   Models/UserModel.o \
	   Models/RecordModel.o \
	   Models/SessionModel.o \
	   Models/SessionContainer.o\
	   Models/ItemModel.o \
	   Models/BannerModel.o \
	   Models/GoodsModel.o \
	   Models/ReplayModel.o \
	   JsonParser/JsonArray.o \
	   JsonParser/JsonObject.o \
	   JsonParser/JsonAtom.o \
	   JsonParser/JsonParser.o \
	   JsonParser/XMLStageParser.o \
	   FBAPI/FBGraph.o \
	   Controllers/UsersController.o \
	   Controllers/FriendsController.o \
	   Controllers/RecordController.o \
	   Controllers/StagesController.o \
	   Controllers/ItemsController.o \
	   Controllers/MarketController.o \
	   Controllers/SessionController.o \
	   Controllers/ReplayController.o \
	   Controller.o \
	   Environment.o \
	   ModelField.o \
	   FieldSet.o \
	   Model.o \
	   Request.o \
	   Response.o \
	   Routes.o \
	   StringHelper.o \
	   Url.o \


test : $(LIBS)
	$(CXX) -g -o $@ $(LIBS) -lmysqlcppconn -I$(LIBBSON) -I$(LIBMONGO)

.PHONY : debug 
debug : $(LIBS)
	$(CXX) -D DEBUG -ggdb -g -o test.fcgi $(LIBS) -lmysqlcppconn  -I$(LIBXML2) -I$(LIBBSON) -I$(LIBMONGO) -lmongoc-1.0 -lboost_system -lbson-1.0 -lfcgi -lxml2 -lcurl -ljson-c -lcrypto $(shell pkg-config --cflags --libs redis3m)

.PHONY : all
all : $(LIBS)
	$(CXX) -o test.fcgi $(LIBS) -lmysqlcppconn  -I$(LIBXML2) -I$(LIBBSON) -I$(LIBMONGO) -lmongoc-1.0 -lboost_system -lbson-1.0 -lfcgi -lxml2 -lcurl -ljson-c -lcrypto $(shell pkg-config --cflags --libs redis3m)

.PHONY : clean
clean :
	rm -rf *.exe *.o *~ *.bak
	rm Controllers/*.o
	rm Models/*.o
	rm Database/*.o
	rm JsonParser/*.o
	rm FBAPI/*.o
	rm test.fcgi
