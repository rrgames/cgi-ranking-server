/*
 * Request.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include <stdlib.h>
#include <string.h>
#include "Request.h"
#include "Url.h"
#include "StringHelper.h"
#include "Controller.h"
#include <fcgi_stdio.h>
#include "LogModule.h"

Request::Request() {

    std::string raw_url = Url::Decode(this->GetEnv(Environment::RequestUri));

    int q_loc = raw_url.find("?");
    std::string real_url;
    if(q_loc > 0) {
        real_url = raw_url.substr(0, q_loc);
    } else {
        real_url = raw_url;
    }
    std::vector<std::string> paths = StringHelper::SplitByToken("/", real_url);
    int pathlen = paths.size();

//	MethodName = "";
    /* URL을 분석해 Rest Method로 변환하는 부분이다.
     * : GET /users/13/friends/testMethod
     * 1. 두번째 부분의 인자를 체크해서 메소드 명인지, Object ID인지 구분한다.
     * 2. 세번째 부분을 컨트롤러 이름으로 등록한다 "friends"
     * 	  이 과정에서 `13`은 users_id라는 키로 params에 등록된다.
     * 3. 마지막 부분을 메소드 명으로 등록한다. 위와같은 Custom method의 경우 RestFul Method는 Unknown으로 등록된다.
     */
// 사실 이거... 컨트롤러가 전담하면 되는 부분이다.
	
	Environment::HttpMethods hM= this->GetHttpMethod();
	
    for (int i = 0; i<pathlen; i++) {
        if(i == 0) {
            // case of level1-method
            params["method_1"] = paths[0];
        }
        if(i == 1) {
            // case of level1-submethod
            params["model_id_1"] = paths[1];

        }
        if(i == 2) {
            // case of level2-method
            params["method_2"] = paths[2];
        }
        if(i == 3) {
            //숫자가 더 들어오면 아마도 show겠지?
            // case of levle2-submethod
            params["model_id_2"] = paths[3];
        }
    }

    errorFlag = false;
    std::string value = Url::Decode(this->GetEnv(Environment::QueryString));

    if(Environment::GetHttpMethod() == Environment::POST ||
            Environment::GetHttpMethod() == Environment::PATCH)
        if (this->GetPostValue() == -1)
            errorFlag = true;

    //만약 특별한 쿼리가 있다면!
    if (value != "") {
        if (this->GenerateParameter(value) == -1) {
            errorFlag = true;
        }
    }
}


bool Request::HasError() {
    return this->errorFlag;
}

std::string Request::GetParam(const std::string paramName) {
    if (params.find(paramName) != params.end()) {
        return params[paramName];
    }
    return "";
}
Environment::RestfulMethods Request::GetRestfulMethod() {
    return this->restMethod;
}

Environment::HttpMethods Request::GetHttpMethod() {
    if (this->GetEnv(Environment::RequestMethod) == "GET")
        return Environment::GET;
    else if (this->GetEnv(Environment::RequestMethod) == "POST")
        return Environment::POST;
    else if (this->GetEnv(Environment::RequestMethod) == "PATCH")
        return Environment::PATCH;
    else if (this->GetEnv(Environment::RequestMethod) == "DELETE")
        return Environment::DELETE;
}

std::string Request::GetEnv(const std::string envName) {
    return std::getenv(envName.c_str());
}

unsigned long Request::GetContentLength() {
    this->contentLength = atoi(Environment::GetEnv(Environment::ContentLength).c_str());
    return this->contentLength;
}

std::string Request::GetMethodName() {
    // 아마 안쓰는 함수일듯.
    return this->methodName;
}
std::string Request::GetControllerName() {
    return this->controllerName;
}

int Request::GetPostValue() {
    // POST일때만 조건부동작함.
	LogModule::GetInstance()->Start();
    char buffer[2048];
    int readed;
    memset(buffer, 0, 2048);
    readed = fread(buffer, 1, 2048, stdin);
    std::string buf;
    if(readed != -1) { // if read succeed.
        buf = Url::Decode(buffer);
	LogModule::GetInstance()->Info("", buf);
        if(this->GenerateParameter(buf) == 0) {
            // succeed
            return 0;
        }
    } else {
        return -1;
    }
    return -1;
}

int Request::GenerateParameter(std::string param_str) {
    std::vector<std::string> pairs;
	StringHelper::SplitByToken("&", param_str, &pairs);
    std::vector<std::string> parts;
    int pairsize = pairs.size();
    if (pairsize <= 0)
	return -1; 
    for(int i = 0; i<pairsize; i++) {
        parts = StringHelper::Split("=", pairs[i]);
        if(parts.size() > 1 && parts[1] != "") {
            params[parts[0]] = Url::Decode(parts[1]);
        } else {
            // 이렇게 안찢어진 데이터가 있단 자체가 잘못된 데이터임.
            return -1;
        }
    }
    return 0;
}
