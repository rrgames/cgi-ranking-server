#ifndef INCLUDE_RESTCLIENT_H_
#define INCLUDE_RESTCLIENT_H_

#include <curl/curl.h>
#include <string>
#include <map>
#include <cstdlib>
#include <algorithm>

class RestClient
{
  public:
    typedef std::map<std::string, std::string> headermap;

    typedef struct
    {
      int code;
      std::string body;
      headermap headers;
    } response;
    typedef struct
    {
      const char* data;
      size_t length;
    } upload_object;

    static void clearAuth();
    static void setAuth(const std::string& user,const std::string& password);
    static response get(const std::string& url);
    static response post(const std::string& url, const std::string& ctype,
                         const std::string& data);
    static response put(const std::string& url, const std::string& ctype,
                        const std::string& data);
    static response del(const std::string& url);

  private:
    static size_t write_callback(void *ptr, size_t size, size_t nmemb,
                                 void *userdata);

    static size_t header_callback(void *ptr, size_t size, size_t nmemb,
				  void *userdata);
    static size_t read_callback(void *ptr, size_t size, size_t nmemb,
                                void *userdata);
    static const char* user_agent;
    static std::string user_pass;
    static inline std::string &ltrim(std::string &s) {
      s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
      return s;
    }
    static inline std::string &rtrim(std::string &s) {
      s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
      return s;
    }
    static inline std::string &trim(std::string &s) {
      return ltrim(rtrim(s));
    }
};

#endif
