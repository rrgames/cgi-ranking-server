/*
 * Url.h
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#ifndef URL_H_
#define URL_H_

#include <string>
#include "StringHelper.h"

class Url {
public:
    static std::string Encode(std::string input);
    static std::string Decode(std::string input);
};

#endif /* URL_H_ */
