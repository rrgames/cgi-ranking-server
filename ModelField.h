/*
 * ModelField.h
 *
 *  Created on: 2014. 9. 21.
 *      Author: bae-unidev
 */

#ifndef MODELFIELD_H_
#define MODELFIELD_H_

#include <string>

class ModelField {
public:
    ModelField(std::string name, int index, std::string typeName);
    ~ModelField();
    std::string fieldName;
    std::string typeName;
    int index;

    enum DataType { Integer, String, };
};

#endif /* MODELFIELD_H_ */
