/*
 * Controller.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "Controller.h"
#include "Controllers/UsersController.h"
#include "Controllers/StagesController.h"
#include "Controllers/RecordController.h"
#include "Controllers/FriendsController.h"
#include "Controllers/SessionController.h"
#include "Controllers/MarketController.h"
#include "Controllers/ItemsController.h"
#include "Controllers/ReplayController.h"

Controller* Controller::CreateController(std::string controllerName) {

    if(controllerName == "users")
        return (Controller*)new UsersController();
    else if(controllerName == "stages")
        return (Controller*)new StagesController();
    else if(controllerName == "records")
        return (Controller*)new RecordController();
    else if(controllerName == "friends")
        return (Controller*)new FriendsController();
    else if(controllerName == "session")
        return (Controller*)new SessionController();
	else if(controllerName == "market")
		return (Controller*)new MarketController();
	else if(controllerName == "items")
		return (Controller*)new ItemsController();
	else if(controllerName == "replay")
		return (Controller*)new ReplayController();
    else
        return NULL;
}

void Controller::RemoveController(Controller* pointer) {
    delete pointer;
}

void Controller::SetRequest(Request* request) {
    this->request = request;
}

Response* Controller::Action() {
    if (request == NULL)
        return NULL;
  // 토큰인증부분을 추가한다.
	switch(Environment::GetHttpMethod()){
		case Environment::GET:
			if(request->GetParam("model_id_1") != "")
				return this->ActionShow();
			return this->ActionIndex();
			break;
		case Environment::POST:
			return this->ActionCreate();
			break;
		case Environment::PATCH:
			return this->ActionUpdate();
			break;
		case Environment::DELETE:
			return this->ActionDelete();
			break;
	}
    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

