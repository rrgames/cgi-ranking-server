//
//  LogModule.cpp
//  JsonBuilder
//
//  Created by ARENA on 2014. 9. 23..
//  Copyright (c) 2014년 ARENA. All rights reserved.
//

#include "LogModule.h"
#include "Environment.h"

LogModule* LogModule::instance = NULL;

std::string backTracer() {
    void * array[10];
    char ** bt_syms;
    size_t size;
    size = backtrace(array, 10);
    bt_syms = backtrace_symbols(array, 10);
    return bt_syms[2];
}

LogModule::LogModule() {
    logFile = fopen("log/logOpen.log", "a");

    if(!logFile) {
        // printf("file open error");
    }
}

LogModule::~LogModule() {
    if(logFile) {
        fclose(logFile);
    }
}

void LogModule::logBuilder(std::string method, std::string logtype, std::string msg) {
    fprintf(logFile, "[%s], [%s], [%s], [%s], delayed: %f sec \n",
            Environment::GetNowTime().c_str(),
            logtype.c_str(),
           	method.c_str(), 
            msg.c_str(),
            ((double)(clock()-(start_points.size()==0? clock() : start_points.back()))/CLOCKS_PER_SEC));
    fflush(logFile);
	if(start_points.size()!=0)
   	 	start_points.pop_back();
}

void LogModule::Debug(std::string method, std::string msg) {
    logBuilder(method, "[DEBUG]", msg);
}

void LogModule::Info(std::string method, std::string msg) {
    logBuilder(method, "[INFO]", msg);
}

void LogModule::Error(std::string method, std::string msg) {
    logBuilder(method, "[ERROR]", msg);
}

void LogModule::Url(std::string url, std::string msg) {

}
void LogModule::Start() {
    start_points.push_back(clock());
}
