/*
 * Model.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "Model.h"
#include "StringHelper.h"

Model::Model() {
    this->selectField = "*";
    this->limitCount = 100;
    FieldSet fieldset;
	this->commitType = EMPTY;
}
Model::~Model() {
}
const std::string Model::GetModelName() {
    return "NULL";
}

const std::string Model::GetTableName() {
    return "NULL";
}

Model* Model::Delete() {
    this->commitType = DELETE;
    return this;
}

Model* Model::Select(std::string select) {
    this->commitType = SELECT;
    this->selectField = select;
    return this;
}

Model* Model::Update() {
	this->commitType = UPDATE;
	return this;
}

Model* Model::New() {
    this->commitType = INSERT;
    fieldset.Clear();
    return this;
}

FieldSet* Model::GetFieldset() {
    return &fieldset;
}

Model* Model::AddValue(std::string columnName, int value) {
    insertMap.insert(std::make_pair(columnName, StringHelper::IntToString(value)));
    return this;
}

Model* Model::AddValue(std::string columnName, std::string value) {
    insertMap.insert(std::make_pair(columnName, "'" + value + "'"));
    return this;
}

Model* Model::Where(std::string where) {
    this->wherePassage = where;
    return this;
}

Model* Model::Limit(int count) {
    this->limitCount = count;
    return this;
}

Model* Model::Commit() {
    fieldset.Clear();

    std::string query;
    if (this->commitType == SELECT) {
        query += "SELECT ";
        query += this->selectField + " FROM " + GetTableName();
        if (!this->wherePassage.empty()) {
            query += " WHERE ";
            query += this->wherePassage;
        }
        query += " LIMIT " + StringHelper::IntToString(this->limitCount);
        query += ";";
        ExecuteQuery(query);
    } else if (this->commitType == INSERT) {
        query += "INSERT INTO " + GetTableName();
        query += " (";

        std::map<std::string, std::string>::iterator iter;
        std::string values = "(";
        for(iter = insertMap.begin(); iter != insertMap.end(); iter++) {
            if (iter != insertMap.begin()) {
                query += ", ";
                values += ", ";
            }
            query += iter->first;
            values += iter->second;
        }
        query += ") VALUES " + values + ");";
        insertMap.clear();
        this->ExecuteNonQuery(query);
    } else if (this->commitType == DELETE) {
        query += "DELETE FROM "  + GetTableName();
        if (!this->wherePassage.empty()) {
            query += " WHERE ";
            query += this->wherePassage;
        }
        query += ";";
        this->ExecuteNonQuery(query);
    } else if (this->commitType == UPDATE) {
		query += "UPDATE ";
		query += GetTableName();
		
		query += " SET ";
		std::map<std::string, std::string>::iterator iter;
		for(iter = insertMap.begin(); iter != insertMap.end(); iter++){
			if(iter != insertMap.begin()){
				query += ", ";
			}
			query += iter->first;
			query += " = ";
			query += iter->second;
		}
		if(!this->wherePassage.empty()){
			query += " WHERE ";
			query += this->wherePassage;
		}
		this->ExecuteNonQuery(query);
	}
    //std::cout << query << std::endl;
	this->commitType = EMPTY;
    return this;
}

void Model::ExecuteNonQuery(std::string query) {
    sql::Connection* conn =  MySQLPool::GetInstance()->GetConnectionFromPool();
    sql::Statement* stmt = conn->createStatement();
    stmt->execute(query);
    delete stmt;
    MySQLPool::GetInstance()->ReleaseConnectionToPool(conn);
}

void Model::ExecuteQuery(std::string query) {
    sql::Connection* conn =  MySQLPool::GetInstance()->GetConnectionFromPool();
    sql::Statement* stmt = conn->createStatement();
    sql::ResultSet* res = stmt->executeQuery(query);
    sql::ResultSetMetaData* meta = res->getMetaData();

    int rowsCount = res->rowsCount();
    int columnsCount = meta->getColumnCount();

    fieldset.Clear();
    fieldset.SetSize(columnsCount, rowsCount);

    for (int i = 0; i < columnsCount; i++) {
        fieldset.SetScheme(meta->getColumnName(i+1), i, meta->getColumnTypeName(i+1));
    }

    fieldset.SetData(res);
    delete res;
    delete stmt;
    MySQLPool::GetInstance()->ReleaseConnectionToPool(conn);
}

int Model::GetRowsCount() {
    return fieldset.GetRowsCount();
}

std::string* Model::GetString(std::string columnName) {
    return fieldset.GetString(fieldset.GetColumnIndex(columnName), 0);
}

std::string* Model::GetString(std::string columnName, int row) {
    return fieldset.GetString(fieldset.GetColumnIndex(columnName), row);
}

long long int* Model::GetInt(std::string columnName) {
    return fieldset.GetInt(fieldset.GetColumnIndex(columnName), 0);
}

long long int* Model::GetInt(std::string columnName, int row) {
    return fieldset.GetInt(fieldset.GetColumnIndex(columnName), row);
}
