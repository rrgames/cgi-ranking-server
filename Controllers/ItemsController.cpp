/*
 * ItemsController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena 
 */

#include "ItemsController.h"
#include "../Models/UserModel.h"
#include "../JsonParser/JsonParser.h"
#include "../LogModule.h"
#include "../Models/ItemModel.h"

const std::string ItemsController::GetControllerName() {
    return "items";
}
const std::string ItemsController::GetModelName() {
    return "item";
}
ItemsController::ItemsController() : Controller() {
    this->request = NULL;
}
Response* ItemsController::ActionIndex() {
	std::string user_id = this->request->GetParam("model_id_1");
	Model * items = new ItemModel();

	std::string where = "user_id = " + user_id;
	items->Select("*")->Where(where)->Commit();

	std::string jsonResult = JsonParser::ModelToJson("items", items);
	delete items;
    return Response::CreateResponse(200, jsonResult);
}
Response* ItemsController::ActionShow() {

    return Response::CreateResponse(400, "not implemented.");
}
Response* ItemsController::ActionUpdate() {
	// 아이템 소비 메서드.
	// PUT 내지는 PATCH를 사용한다.
	std::string user_id = this->request->GetParam("model_id_1"); 
	std::string quantity = this->request->GetParam("quantity");
	std::string item_id = this->request->GetParam("model_id_2");
	Model * items = new ItemModel();

	std::string where = "user_id = " + user_id + ",";
	where += "and item_id = " + item_id;

	items->Update()
		->AddValue("quantity", quantity)
		->Where(where)
		->Commit();	
    return Response::CreateResponse(200, "Item consumption complete"); 
}
Response* ItemsController::ActionDelete() {
    // 삭제 연산 없음.
    return NULL;
}
Response* ItemsController::ActionCreate() {
    // Facebook연동시에 사용.
    return Response::CreateResponse(200, "will be created soon.");
}

Response* ItemsController::Action() {
    if (request == NULL)
        return NULL;

	switch(Environment::GetHttpMethod()){
		case Environment::GET:
			return this->ActionIndex();
			break;
		case Environment::POST:
			return this->ActionCreate();
			break;
		case Environment::PATCH:
			return this->ActionUpdate();
			break;
		case Environment::DELETE:
			return this->ActionDelete();
			break;
	}

    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

