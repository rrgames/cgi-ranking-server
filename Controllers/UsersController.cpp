/*
 * UsersController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "UsersController.h"
#include "../Models/UserModel.h"
#include "../JsonParser/JsonParser.h"
#include "../LogModule.h"
#include "../GAModule.h"

const std::string UsersController::GetControllerName() {
    return "users";
}
const std::string UsersController::GetModelName() {
    return "user";
}
UsersController::UsersController() : Controller() {
    this->request = NULL;
}
Response* UsersController::ActionIndex() {
    LogModule::GetInstance()->Start();
    UserModel *user = new UserModel();
    user->Select("*")->Limit(100000)->Commit();
    std::string jsonResult = JsonParser::ModelToJson("users", user);
    delete user;
    LogModule::GetInstance()->Debug("", "JSON and QUERY");
    return Response::CreateResponse(200, jsonResult);
}
Response* UsersController::ActionShow() {

    LogModule::GetInstance()->Start();

    UserModel * user = new UserModel();
    user->Select("*")->Where("id = '" + this->request->GetParam("model_id_1") + "'")->Limit(1)->Commit();

    std::string jsonResult = JsonParser::ModelToJson("user", user);
    delete user;
    LogModule::GetInstance()->Debug("", "JSON and QUERY");
    return Response::CreateResponse(200, jsonResult);
}
Response* UsersController::ActionUpdate() {
    // Update 연산 없음.
    return NULL;
}
Response* UsersController::ActionDelete() {
    // 삭제 연산 없음.
    return NULL;
}
Response* UsersController::ActionCreate() {
    // Facebook연동시에 사용.
    return Response::CreateResponse(200, "will be created soon.");
}
