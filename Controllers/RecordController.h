/*
 * UsersController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#ifndef RECORDCONTROLLER_H_
#define RECORDCONTROLLER_H_

#include "../Controller.h"
#include <stdio.h>
#include "../JsonParser/JsonParser.h"

class RecordController : public Controller {
public:
    RecordController();
    virtual	const std::string GetControllerName() ;
    virtual const std::string GetModelName() ;

protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();
	virtual Response* Action();
};

#endif /* USERSCONTROLLER_H_ */
