/*
 * MarketController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#ifndef MARKETCONTROLLER_H_
#define MARKETCONTROLLER_H_

#include "../Controller.h"

class MarketController : public Controller {
public:
    MarketController();
    virtual	const std::string GetControllerName() ;
    virtual const std::string GetModelName() ;

protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();
	virtual Response* Action();

private:
	Response* ActionShowBanners();
	Response* ActionShowGoods();
	Response* ActionBuy();
	Response* ActionChargeCash();
};

#endif /* USERSCONTROLLER_H_ */
