/*
 * MarketController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#include "MarketController.h"
#include "../Models/UserModel.h"
#include "../JsonParser/JsonParser.h"
#include "../LogModule.h"
#include "../Models/ItemModel.h"
#include "../Models/GoodsModel.h"
#include "../Models/BannerModel.h"
#include "../Models/SessionContainer.h"

const std::string MarketController::GetControllerName() {
    return "market";
}
const std::string MarketController::GetModelName() {
    return "none";
}
MarketController::MarketController() : Controller() {
    this->request = NULL;
}

// 일반적인 Resource CRUD 를 수행하는 것이 아님.
Response* MarketController::ActionIndex() {
    return Response::CreateResponse(200, "none");
}
Response* MarketController::ActionShow() {
    return Response::CreateResponse(200, "none");
}
Response* MarketController::ActionUpdate() {
    return NULL;
}
Response* MarketController::ActionDelete() {
    return NULL;
}
Response* MarketController::ActionCreate() {
	return NULL;
}

Response* MarketController::ActionShowBanners() {
	Model * banner = new BannerModel();
	banner->Select("*")->Commit();
	std::string jsonResult = JsonParser::ModelToJson("banner", banner);
	delete banner;
	return Response::CreateResponse(200, jsonResult);
}

Response* MarketController::ActionShowGoods() {
	std::string category = this->request->GetParam("category");
	std::string id = this->request->GetParam("id");

	Model * goods = new GoodsModel();
	std::string where;
	if(category != ""){
		where = "category = '" + category + "'";
		goods->Select("*")->Where(where)->Commit();
	}
	else if(id != ""){
		where = "id = '" + id + "'";
		goods->Select("*")->Where(where)->Commit();
	}
	else{
		goods->Select("*")->Commit();
	}

	std::string jsonResult = JsonParser::ModelToJson("goods", goods);
	delete goods;

	return Response::CreateResponse(200, jsonResult);
}

Response* MarketController::ActionBuy() {
    std::string user_id = StringHelper::IntToString(SessionContainer::GetInstance()->GetUserID(this->request->GetParam("token")));
    if (user_id == "-1") {
       return Response::CreateResponse(401, "auth is fuck");
    }
	std::string quantity = this->request->GetParam("quantity");
	std::string item_id = this->request->GetParam("item_id");
	std::string jsonResult;
	/// 만약 저 셋중에 하나라도 안들어오면 에러니까 에러처리하자.
	// 물론 나중에....
	int quant = atoi(quantity.c_str());

	Model * user = new UserModel();
	Model * item = new ItemModel();
	std::string where = "id = " + user_id;
	user->Select("money_jjo, money_ping")->Where(where)->Commit();

	int jjo = *user->GetInt("money_jjo");
	int ping = *user->GetInt("money_ping");

	Model * goods = new GoodsModel();
	where = "id = " + item_id;
	goods->Select(" price_jjo, price_ping, discount ")->Where(where)->Commit();

	int priceJjo = *goods->GetInt("price_jjo");
	int pricePing = *goods->GetInt("price_ping");
	int prev_got = 0;

	if(priceJjo * quant > jjo || pricePing * quant > ping){
		// 돈이 없다?
		jsonResult = "{\"message\": \"You have no Money\"}";
	}
	else{
		// 사용자 돈을 까기.
		jjo = jjo - (priceJjo * quant);
		ping = ping - (pricePing * quant);
		where = "id = " + user_id;
		user->Update()->Where(where)->
			AddValue("money_jjo", jjo)->
			AddValue("money_ping", ping)->
			Commit();
		// 아이템 채워주기.
		where = "user_id = " + user_id + " and item_id = " + item_id;
		
		item->Select("*")->Where(where)->Commit();
		if(item->GetRowsCount()==0){
			// 전에 이런 아이템이 없었다?
			item->New()->
				AddValue("user_id", user_id)->
				AddValue("item_id", item_id);
		}else{
			prev_got = *item->GetInt("quantity", 0);
			item->Update();
		}
		item->AddValue("quantity", prev_got + quant)->
			Commit();
	
		jsonResult = "{ \"money_jjo\":\""+ StringHelper::IntToString(jjo)+"\", \"money_ping\":"+StringHelper::IntToString(ping)+ "}";	
	}

	delete user;
	delete goods;
	delete item;
	
	return Response::CreateResponse(200, jsonResult);
}
Response* MarketController::ActionChargeCash() {

}

Response* MarketController::Action() {
    if (request == NULL)
        return NULL;
    // 토큰인증부분을 추가한다.
    switch(request->GetHttpMethod()) {
	case Environment::GET:
		// GET 메서드로 루트되는것,
		// show_banner, show_goods
		if(this->request->GetParam("model_id_1") == "goods")
			return this->ActionShowGoods();
		else if(this->request->GetParam("model_id_1") == "banner")
			return this->ActionShowBanners();
		break;
	case Environment::POST:
		// POST METHOD 로 로트되는것.
		// 구매요청, 쪼핑충전.
		if(this->request->GetParam("model_id_1") == "buy")
			return this->ActionBuy();
		else if(this->request->GetParam("model_id_1") == "cash")
			return this->ActionChargeCash();
		break;
	case Environment::PATCH:
		break;
	case Environment::DELETE:
		break;
    }

    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

