/*
 * UsersController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#ifndef USERSCONTROLLER_H_
#define USERSCONTROLLER_H_

#include "../Controller.h"

class UsersController : public Controller {
public:
	UsersController();
    virtual	const std::string GetControllerName() ;
    virtual const std::string GetModelName() ;

private:
	static UsersController * instance;

protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();


};

#endif /* USERSCONTROLLER_H_ */
