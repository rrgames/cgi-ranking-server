/*
 * UsersController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#ifndef STAGESCONTROLLER_H_
#define STAGESCONTROLLER_H_
#include "../Controller.h"
#include <fcgi_stdio.h>
class StagesController : public Controller {
public:
    StagesController();
    virtual	const std::string GetControllerName() ;
    virtual const std::string GetModelName() ;
    virtual Response* Action();

protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();

    Response* showXMLData();

};

#endif /* STAGESCONTROLLER_H_ */
