/*
 * ReplayController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena 
 */

#include "ReplayController.h"
#include "../Models/UserModel.h"
#include "../JsonParser/JsonParser.h"
#include "../LogModule.h"
#include "../Models/ReplayModel.h"
#include "../Models/SessionContainer.h"
#include "../Environment.h"
const std::string ReplayController::GetControllerName() {
    return "replays";
}
const std::string ReplayController::GetModelName() {
    return "replay";
}
ReplayController::ReplayController() : Controller() {
    this->request = NULL;
}
Response* ReplayController::ActionIndex() {

	std::string user_id = StringHelper::IntToString(SessionContainer::GetInstance()->GetUserID(this->request->GetParam("token")));
	if(user_id == "-1")
		return Response::CreateResponse(401, "auth failed.");
	std::string where = "user_id = " + this->request->GetParam("model_id_1");

	Model * replay = new ReplayModel();
	replay->Select("*")->Where(where)->Limit(50)->Commit();
	std::string jsonResult =  JsonParser::ModelToJson("replay", replay);

	delete replay;
    return Response::CreateResponse(200,jsonResult);
}
Response* ReplayController::ActionShow() {
	std::string user_id = StringHelper::IntToString(SessionContainer::GetInstance()->GetUserID(this->request->GetParam("token")));
	if(user_id == "-1")
		return Response::CreateResponse(401, "auth failed.");
	std::string id = this->request->GetParam("model_id_2");
	std::string where = "user_id = " + this->request->GetParam("model_id_1")+" and id = " + id;

	Model * replay = new ReplayModel();
	replay->Select("*")->Where(where)->Limit(1)->Commit();
	std::string jsonResult =  JsonParser::ModelToJson("replay", replay);

	delete replay;
    return Response::CreateResponse(400, jsonResult);
}
Response* ReplayController::ActionUpdate() {
	// 아이템 소비 메서드.
	// PUT 내지는 PATCH를 사용한다.
    return Response::CreateResponse(200, "not implemented"); 
}
Response* ReplayController::ActionDelete() {
    // 삭제 연산 없음.
    return NULL;
}
Response* ReplayController::ActionCreate() {
	std::string user_id = StringHelper::IntToString(SessionContainer::GetInstance()->GetUserID(this->request->GetParam("token")));
	if(user_id == "-1")
		return Response::CreateResponse(401, "auth failed.");
	std::string data = this->request->GetParam("data");
	Model * replay = new ReplayModel();
	replay->New()->AddValue("data", data)->
		AddValue("user_id", user_id)->
		AddValue("created_at", Environment::GetNowTime())->
		AddValue("updated_at", Environment::GetNowTime())->
		Commit();
	delete replay;
    return Response::CreateResponse(200, "completed");
}

Response* ReplayController::Action() {
    if (request == NULL)
        return NULL;

	switch(Environment::GetHttpMethod()){
		case Environment::GET:
			if(request->GetParam("model_id_2") != "")
				return this->ActionShow();
			return this->ActionIndex();
			break;
		case Environment::POST:
			return this->ActionCreate();
			break;
		case Environment::PATCH:
			return this->ActionUpdate();
			break;
		case Environment::DELETE:
			return this->ActionDelete();
			break;
	}

    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}



