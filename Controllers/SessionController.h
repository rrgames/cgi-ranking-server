/*
 * SessionController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#ifndef SESSIONCONTROLLER_H_
#define SESSIONCONTROLLER_H_

#include "../Controller.h"

class SessionController : public Controller {
public:
    SessionController();
    virtual	const std::string GetControllerName();
    virtual const std::string GetModelName();

protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();

};

#endif /* SessionCONTROLLER_H_ */
