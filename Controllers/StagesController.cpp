/*
 * StagesController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#include "StagesController.h"
#include "../Models/StageModel.h"
#include "../JsonParser/XMLStageParser.h"
#include "../LogModule.h"
#include <string.h>
#include <stdio.h>
const std::string StagesController::GetControllerName() {
    return "stages";
}
const std::string StagesController::GetModelName() {
    return "stage";
}
StagesController::StagesController() : Controller() {
    this->request = NULL;
}
Response* StagesController::ActionIndex() {
    Model * stage = new StageModel();
    stage->Select("*")->Limit(100)->Commit();

    std::string res = JsonParser::ModelToJson("stages", stage);
    delete stage;
    return Response::CreateResponse(200, res);
}
Response* StagesController::ActionShow() {
    Model * stage = new StageModel();
    stage->Select("*")->Where("file_name = '" + this->request->GetParam("model_id_1") + "'")->Limit(100)->Commit();
    std::string res = JsonParser::ModelToJson("stage", stage);
    delete stage;
    return Response::CreateResponse(200, res);

}
Response* StagesController::ActionUpdate() {
    // update 사용 안함.
    return NULL;
}
Response* StagesController::ActionDelete() {
    // delete 사용 안함.
    return NULL;
}
Response* StagesController::ActionCreate() {
    // 스테이지 파일 삽입한다.
    // 필요한 파라미터는, stage_id, xml
    std::string stage_id = this->request->GetParam("stage_id");
    std::string file = "stageXML/" + stage_id + ".xml";
    std::string xml = this->request->GetParam("xml");
    FILE * ofile = fopen(file.c_str(), "w");
    // XML쓰기
    fprintf(ofile, "%s", xml.c_str());
    fclose(ofile);
    ///XML 파싱 이후 DB삽입도 필수....
    XMLStageParser *parser = new XMLStageParser(file);
    // 자주 쓰는것이 아니므로 성능문제는 고려하지 않는다.

    Model * stage = new StageModel();
	// 스테이지 찾기.
	// 있다 -> 정보 업데이트만
	// 없다 -> 정보 삽입!
	std::string where = " file_name = '";
	where += stage_id + "'";
	stage->Select("*")->Where(where)->Commit();

	if(stage->GetRowsCount()==0){
	    stage->New();
	}else{
		stage->Update()->Where(where);
	}
	
	stage->
	AddValue("grade_one", parser->ParseStageXML("L1"))->
    AddValue("grade_two", parser->ParseStageXML("L2"))->
 	AddValue("grade_three", parser->ParseStageXML("L3"))->
   	AddValue("comment", parser->ParseStageXML("description"))->
   	AddValue("total_load", 0)->
   	AddValue("ruletype", parser->ParseStageXML("missions"))->
   	AddValue("file_name", stage_id)->
	AddValue("created_at", Environment::GetNowTime())->
	AddValue("updated_at", Environment::GetNowTime())->
   	Commit();

    delete stage;

    return Response::CreateResponse(200, "fine");
}

Response* StagesController::showXMLData() {
    //xml 정보를 보여준다.

    std::string stage_id = this->request->GetParam("model_id_1");
    std::string file = "stageXML/" + stage_id+ ".xml";
    FILE * ofile = fopen(file.c_str(), "r");
    if(ofile) {
        char buffer[2048];
        memset(buffer, 0, 2048);
        fread(buffer, 1, 2048, ofile);
        fclose(ofile);
        std::string result = buffer;
        return Response::CreateResponse(200,result);
    }
    return Response::CreateResponse(404, "Not Found.");
}
Response* StagesController::Action() {
    if (request == NULL)
        return NULL;

	switch(Environment::GetHttpMethod()){
		case Environment::GET:
			if(request->GetParam("method_2")=="xml_data")
				return this->showXMLData();
			if(request->GetParam("model_id_1") != "")
				return this->ActionShow();
			return this->ActionIndex();
			break;
		case Environment::POST:
			return this->ActionCreate();
			break;
		case Environment::PATCH:
			return this->ActionUpdate();
			break;
		case Environment::DELETE:
			return this->ActionDelete();
			break;
	}

    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}
