/*
 * ItemsController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: bae-unidev
 */

#ifndef ITEMSCONTROLLER_H_
#define ITEMSCONTROLLER_H_

#include "../Controller.h"

class ItemsController : public Controller {
public:
    ItemsController();
    virtual	const std::string GetControllerName() ;
    virtual const std::string GetModelName() ;
	virtual Response* Action();
protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();

};

#endif /* USERSCONTROLLER_H_ */
