#ifndef _REPLAY_CONTROLLER_H_
#define _REPLAY_CONTROLLER_H_

#include "../Controller.h"

class ReplayController : public Controller {
	public:
		ReplayController();
		virtual const std::string GetControllerName();
		virtual const std::string GetModelName();
		virtual Response* Action();
	protected:
		Response* ActionIndex();
		Response* ActionShow();
		Response* ActionUpdate();
		Response* ActionDelete();
		Response* ActionCreate();
};

#endif
