/*
 * FriendsController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#include "FriendsController.h"
#include "../Models/FriendDAO.h"
#include "../Models/FriendModel.h"
#include "../Models/UserModel.h"
#include "../LogModule.h"
const std::string FriendsController::GetControllerName() {
    return "friends";
}
const std::string FriendsController::GetModelName() {
    return "friend";
}
FriendsController::FriendsController() : Controller() {
    this->request = NULL;
}

Response* FriendsController::ActionIndex() {
    std::string user_id = this->request->GetParam("model_id_1");
    //
    // friends 테이블을 이용해서,
    // user 들의 결과를 불러오면 된다.

    // 사용자 아이디로 친구 아이디 리스틀 꺼내온다.
    // 이것을 통해 user 테이블에서 '친구의 신상명세'를 꺼내온다.
    // 이때 where절에 여러 친구가 들어갈 수 있도록 해야 할 듯 함.
	
    FriendDAO * fDAO = new FriendDAO();
    std::vector<FriendModel*> fm = fDAO->FindByUserID(user_id);

    std::string where = "id in (";
    for(int i = 0; i< fm.size(); i++) {
        if (i != 0)
            where += ", ";
        where += fm[i]->GetFriendID();
    }
    where += ")";

    // 객체 등록 삭제.
    for(int i = 0; i< fm.size(); i++) {
        delete fm[i];
    }

    UserModel* user = new UserModel("friend");
    //불러오는 것은 일단 Friend임.
    user->Select("*")->Where(where)->Limit(100)->Commit();

    std::string res = JsonParser::ModelToJson("friends", user);
    delete user;
    delete fDAO;
    return Response::CreateResponse(200, res);

}
Response* FriendsController::ActionShow() {
    return NULL;
}
Response* FriendsController::ActionUpdate() {
    // 친구 목록을 갱신한다.
    // 이때 이전 친구 목록을 삭제하고
    /// 새 친구 목록을 얻어온다.
    return NULL;
}
Response* FriendsController::ActionDelete() {
    // params으로 id, friend_id가 들어온다.
    // 친구 목록을 삭제한다. 하나만..
    return NULL;
}
Response* FriendsController::ActionCreate() {
    return Response::CreateResponse(200, "it works.");
}
Response* FriendsController::Action() {
    if (request == NULL)
        return NULL;
  // 토큰인증부분을 추가한다.
	switch(Environment::GetHttpMethod()){
		case Environment::GET:
			if(request->GetParam("model_id_2") != "")
				return this->ActionShow();
			return this->ActionIndex();
			break;
		case Environment::POST:
			return this->ActionCreate();
			break;
		case Environment::PATCH:
			return this->ActionUpdate();
			break;
		case Environment::DELETE:
			return this->ActionDelete();
			break;
	}
    return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

