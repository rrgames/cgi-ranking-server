/*
 * StagesController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#include <vector>

#include "RecordController.h"

#include "../Models/RecordModel.h"
#include "../Models/StageModel.h"
#include "../Models/FriendDAO.h"
#include "../Models/FriendModel.h"
#include "../Models/SessionContainer.h"
#include "../Database/REDISPool.h"
#include "../Models/UserModel.h"
#include "../GAModule.h"
#include "../LogModule.h"

using namespace redis3m;

const std::string RecordController::GetControllerName() {
    return "records";
}
const std::string RecordController::GetModelName() {
    return "record";
}
RecordController::RecordController() : Controller() {
    this->request = NULL;
}
Response* RecordController::ActionIndex() {
    // 현 스테이지 랭킹 보여준다.
    std::string stageId = this->request->GetParam("model_id_1");
    std::string location  = this->request->GetParam("location");
    std::string limit = this->request->GetParam("limit");
    std::string res = "{ \"records\" : [ ";

    std::string user_id = StringHelper::IntToString(SessionContainer::GetInstance()->GetUserID(this->request->GetParam("token")));
    if (user_id == "-1") {
       return Response::CreateResponse(401, "auth is fuck");
    }
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
	
	if (!location.empty()) {
		FriendDAO* friendDao = new FriendDAO();
		std::vector<FriendModel*> friends = friendDao->FindByUserID(location);
		std::vector<FriendModel*>::iterator iter;
		for(iter = friends.begin(); iter != friends.end(); iter++) {
			if (conn->run(command("EXISTS") << "users:" + ((*iter)->GetFriendID()) + ":" + stageId).integer() == 1) {
				std::string rkey = "users:" + ((*iter)->GetFriendID()) + ":" + stageId;
				std::string jsonObj = conn->run(command("GET") << "records:" + conn->run(command("GET") << rkey).str() + ":data").str();
				res += jsonObj + ",";
			}
		}
		delete friendDao;
	}
	// 자기 랭킹 불러오기.
	std::string mine = conn->run(command("GET") << "records:" + conn->run(command("GET") << "users:" + user_id + ":" + stageId).str() + ":data").str();

	if(mine == ""){
		res.erase(res.size()-1);
	}else{
		res += mine;
	}

	RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
	res += "]}";

	return Response::CreateResponse(200, res);
}
Response* RecordController::ActionShow() {
	// Show메서드 없음.
	return NULL;
}
Response* RecordController::ActionUpdate() {
	// update 메서드 없음.
	return NULL;
}
Response* RecordController::ActionDelete() {
	// delete 메서드 없음.
	return NULL;
}
Response* RecordController::ActionCreate() {
    std::string user_id = StringHelper::IntToString(SessionContainer::GetInstance()->GetUserID(this->request->GetParam("token")));
    if (user_id == "-1") {
       return Response::CreateResponse(401, "authorization failed.");
    }
    std::string top_rank = this->request->GetParam("top_rank");
    std::string stage_id = this->request->GetParam("model_id_1");
    std::string played_turns = this->request->GetParam("played_turns");
    std::string score = this->request->GetParam("score");
    std::string play_time = this->request->GetParam("play_time");
    //std::string played_game_time = Enviroment::

	std::string reward_ping = this->request->GetParam("money_ping");
	Model * user = new UserModel();
	std::string where = "id = "+ user_id;
	user->Select("money_ping, top_rank")->Where(where)->Commit();
	if(reward_ping != ""){
		int ping = *user->GetInt("money_ping", 0);
		ping = ping + atoi(reward_ping.c_str());
		user->Update()->Where(where)->AddValue("money_ping", ping);
		LogModule::GetInstance()->Debug("", reward_ping);
	}
	if(top_rank != ""){
		user->Update()->AddValue("top_rank", stage_id);
	}
	user->Commit();
	delete user;

    std::string jsonData = "{";
    jsonData += "\"stage_id\" : \"" + stage_id + "\"";
    jsonData += ", \"user_id\" : " + user_id;
    jsonData += ", \"played_turns\" : " + played_turns;
    jsonData += ", \"score\" : " + score;
    jsonData += ", \"play_time\" : " + play_time;
    jsonData += "}";

	if(this->request->GetParam("refresh")!="false"){
		connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
		// this is just making unique key.
		std::string recordID = StringHelper::IntToString(conn->run(command("INCR") << "record_loop").integer());
		// 이미 존재한다면
		if (conn->run(command("EXISTS") << "users:" + user_id + ":" + stage_id).str() == "1") {
			std::string oldRecordID = conn->run(command("GET") << "users:" + user_id + ":" + stage_id).str();
			conn->run(command("ZREM") << "stages:" + stage_id << oldRecordID);
			conn->run(command("DEL") << "records:" + oldRecordID);
		}
		conn->run(command("SET") << "users:" + user_id + ":" + stage_id << recordID);
		// 여기서 유저가 이미 플레이한지 알수 있음 EXKEY인가 여튼
		// 그리고
		conn->run(command("SET") << "records:" + recordID + ":data" << jsonData);
		conn->run(command("ZADD") << "stages:" + stage_id << score << recordID);

		RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
	}
	GAModule ga;
	std::string rID = "stages/"+ stage_id + "/records/";
	ga.Commit(rID,user_id);
	return Response::CreateResponse(201, jsonData);
}
Response* RecordController::Action() {
	if (request == NULL)
		return NULL;
	// 토큰인증부분을 추가한다.
	switch(Environment::GetHttpMethod()){
		case Environment::GET:
			if(request->GetParam("model_id_2") != "")
				return this->ActionShow();
			return this->ActionIndex();
			break;
		case Environment::POST:
			return this->ActionCreate();
			break;
		case Environment::PATCH:
			return this->ActionUpdate();
			break;
		case Environment::DELETE:
			return this->ActionDelete();
			break;
	}
	return Response::CreateResponse(500, "{ message : 'fucking restful method' }");
}

