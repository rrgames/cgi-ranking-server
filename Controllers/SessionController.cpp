/*
 * SessionController.cpp
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#include "SessionController.h"
#include "../FBAPI/FBGraph.h"
#include "../Models/UserModel.h"
#include "../Models/SessionContainer.h"
#include "../Models/FriendDAO.h"
#include "../LogModule.h"

const std::string SessionController::GetControllerName() {
    return "sessions";
}
const std::string SessionController::GetModelName() {
    return "session";
}
SessionController::SessionController() : Controller() {
    this->request = NULL;
}
Response* SessionController::ActionIndex() {
	LOG_START;
    std::string code = this->request->GetParam("code");
    std::string platform = this->request->GetParam("platform");

	LogModule::GetInstance()->Start();
    if(code.empty()) {
        return Response::CreateResponse(404, "No code, wrong.");
    }
    std::string access_token;
    if(platform == "flash") {
        access_token = FBApp::GetAccessToken(code);
    } else {
        access_token = code;
    }
	LogModule::GetInstance()->Debug("", access_token);
    FBGraph * graph = new FBGraph(access_token);
    graph->SetMyInfo();

    Model* users = new UserModel();
    std::string where = "facebook_uid = '" + graph->GetMyElement("id") + "'";

    users->Select("*")->Where(where)->Commit();
    if(users->GetRowsCount()==0) {
        // 만약, 첫유저다?
        users->New()->
        AddValue("gender", graph->GetMyElement("gender"))->
        AddValue("facebook_uid", graph->GetMyElement("id"))->
        AddValue("name", graph->GetMyElement("name"))->
        AddValue("profile_image", graph->GetProfilePhotoPath())->
        Commit();

    } else {
        // 새 유저가 아니다?
        // 그럼 프사만 업데이트하고 끝내기.
		users->Update()->Where(where)->
			AddValue("profile_image", graph->GetProfilePhotoPath())->
			Commit();
    }
    users->Select("*")->Where(where)->Commit();
    std::string userId = StringHelper::IntToString(*users->GetInt("id", 0));

    std::string sessionJson = SessionContainer::GetInstance()->GetSessionData(*users->GetInt("id"));
    if (sessionJson == "") { // 가져오지 못했다
        SessionModel * sm = new SessionModel(
            Environment::GetEnv(Environment::RemoteAddr),
            Environment::GetNowTime(), userId,
            access_token);
        sessionJson = sm->ToJsonString();
        SessionContainer::GetInstance()->AppendSession(sm);
        delete sm;
    }

    // 아래는 FriendList를 갱신하는 것이다.
    std::vector<std::string> f_list = graph->GetFriendList();

    // 그중에 우리 앱에 있는 확인하자.
    std::string where_f = "facebook_uid in ( ";
    for(int i = 0; i< f_list.size(); i++) {
        if(i != 0)
            where_f += ", ";
        where_f += f_list[i];
    }
    where_f+=")";

    // 일단 친구목록 무차별 삭제.
    FriendDAO *fdao = new FriendDAO();
    FriendModel fm;
    //delete users;
    //Model * ux = new UserModel();

    fdao->EraseByMyID(userId);
    // 친구들의 '진'아이디를 찾자..
    if(f_list.size() > 0) {
        users->Select("id")->Where(where_f)->Limit(150)->Commit();
        for(int i = 0; i < users->GetRowsCount(); i++) {
            fm.SetMyID(userId);
            fm.SetFriendID(StringHelper::IntToString(*users->GetInt("id", i)));
            fdao->Insert(fm);
        }
    }
    delete fdao;
    delete users;

	MSG_INFO("", "session process.");
   return Response::CreateResponse(200, sessionJson);
	
   // return Response::CreateResponse(200, access_token);
}
Response* SessionController::ActionShow() {
    return NULL;
}
Response* SessionController::ActionUpdate() {
    // Update 연산 없음.
    return NULL;
}
Response* SessionController::ActionDelete() {
    // 삭제 연산 없음.
    return NULL;
}
Response* SessionController::ActionCreate() {
    return Response::CreateResponse(200, "will be created soon.");
}

