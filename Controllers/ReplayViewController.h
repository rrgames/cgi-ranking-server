#ifndef _REPLAY_CONTROLLER_H_
#define _REPLAY_CONTROLLER_H_

#include "../Controller.h"

class ReplayViewController : public Controller {
	public:
		ReplayViewController();
		virtual const std::string GetControllerName();
		virtual const std::string GetModelName();
	protected:
		Response* ActionIndex();
		Response* ActionShow();
		Response* ActionUpdate();
		Response* ActionDelete();
		Response* ActionCreate();
};

#endif
