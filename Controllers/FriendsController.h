/*
 * UsersController.h
 *
 *  Created on: 2014. 9. 7.
 *      Author: into_arena
 */

#ifndef FRIENDSCONTROLLER_H_
#define FRIENDSCONTROLLER_H_

#include "../Controller.h"
#include "../JsonParser/JsonParser.h"
#include <stdio.h>

class FriendsController : public Controller {
public:
    FriendsController();
    virtual const std::string GetControllerName() ;
    virtual const std::string GetModelName() ;

protected:
    Response* ActionIndex();
    Response* ActionShow();
    Response* ActionUpdate();
    Response* ActionDelete();
    Response* ActionCreate();
	virtual Response* Action();
};

#endif /* USERSCONTROLLER_H_ */
