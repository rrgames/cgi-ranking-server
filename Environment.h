/*
* Environment.h
*
*  Created on: 2014. 9. 7.
*      Author: bae-unidev
*/

#ifndef ENVIRONMENT_H_
#define ENVIRONMENT_H_

#include <stdlib.h>
#include <string>
#include <iostream>
using namespace std;

class Environment {
public:
    static const char* DocumentRoot;
    static const char* ContentLength;
    static const char* ContentType;
    static const char* HttpCookie;
    static const char* HttpHost;
    static const char* HttpReferer;
    static const char* HttpUserAgent;
    static const char* Https;
    static const char* Path;
    static const char* QueryString;
    static const char* RemoteAddr;
    static const char* RemoteHost;
    static const char* RemotePort;
    static const char* RemoteUser;
    static const char* RequestMethod;
    static const char* RequestUri;
    static const char* ScriptFilename;
    static const char* ScriptName;
    static const char* ServerAdmin;
    static const char* ServerName;
    static const char* ServerPort;
    static const char* ServerSoftware;

    enum HttpMethods { GET = 0, POST, DELETE, PATCH };

    enum RestfulMethods { Index = 0, Show, Create, Update, Delete, Unknown };

    static HttpMethods GetHttpMethod();
    static std::string GetEnv(const char* envName);
    static std::string GetNowTime();
};


#endif /* ENVIRONMENT_H_ */
