#!/bin/sh

sudo service lighttpd stop
make all
sudo cp test.fcgi /var/www/cgi/
sudo cp cgi_config/10-cgi-lgt.conf /etc/lighttpd/conf-enabled/10-cgi.conf
sudo service lighttpd start

