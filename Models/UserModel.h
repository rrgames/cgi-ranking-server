/*
 * UserModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef USERMODEL_H_
#define USERMODEL_H_

#include "../Model.h"

class UserModel : public Model {
private:
	std::string modelName;
public:
    UserModel();
	UserModel(std::string _name);
    ~UserModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
};

#endif /* USERMODEL_H_ */
