/*
 * ReplayModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: into_arena
 */

#ifndef ReplayMODEL_H_
#define ReplayMODEL_H_

#include "../Model.h"

class ReplayModel : public Model {
public:
    ReplayModel();
    ~ReplayModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
};

#endif /* ReplayMODEL_H_ */
