/*
 * UserModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "UserModel.h"

UserModel::UserModel() : Model() {
	modelName = "user";
}
UserModel::~UserModel() {
}
UserModel::UserModel(const std::string _name) : Model(){
	modelName = _name;
}
const std::string UserModel::GetModelName() {
	return modelName;
}
const std::string UserModel::GetTableName() {
    return "users";
}
