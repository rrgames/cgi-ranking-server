/*
 * UserModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef STAGEMODEL_H_
#define STAGEMODEL_H_

#include "../Model.h"

class StageModel : public Model {
public:
    StageModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
    Model* Create();

};

#endif /* STAGEMODEL_H_ */
