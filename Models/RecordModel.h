/*
 * RecordModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef RECORDMODEL_H_
#define RECORDMODEL_H_

#include "../Model.h"

class RecordModel : public Model {
public:
    RecordModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
    Model* Create();
};

#endif /* RECORDMODEL_H_ */
