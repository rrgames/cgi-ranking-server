/*
 * ReplayModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "ReplayModel.h"

ReplayModel::ReplayModel() : Model() {
}
ReplayModel::~ReplayModel() {
}
const std::string ReplayModel::GetModelName() {
	return "replays";
}
const std::string ReplayModel::GetTableName() {
    return "replays";
}
