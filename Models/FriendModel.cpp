
#include "FriendModel.h"


void FriendModel::SetMyID(std::string id) {
    my_id = id;
}

std::string FriendModel::GetMyID() {
    return my_id;
}

void FriendModel::SetFriendID(std::string id) {
    friend_id = id;
}

std::string FriendModel::GetFriendID() {
    return friend_id;
}
