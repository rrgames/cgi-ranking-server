/*
 * BannerModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "BannerModel.h"

BannerModel::BannerModel() : Model() {
}
BannerModel::~BannerModel() {
}
const std::string BannerModel::GetModelName() {
	return "banner";
}
const std::string BannerModel::GetTableName() {
    return "banners";
}
