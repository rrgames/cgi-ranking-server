#ifndef _SESSION_CONTAINER_H_
#define _SESSION_CONTAINER_H_

#include "SessionModel.h"

#include <vector>
#include <string>
#include "../Database/REDISPool.h"
#include "../StringHelper.h"

using namespace redis3m;

class SessionContainer {
public:
    static SessionContainer * GetInstance() {
        if(!instance) {
            instance = new SessionContainer();
        }
        return instance;
    }
    SessionContainer();
    ~SessionContainer();
    void AppendSession(SessionModel* sm);
    int GetUserID(std::string session);
    std::string GetFacebookTokenFindBySession(std::string session);
    std::string GetSessionData(int userID);

private:
    static SessionContainer* instance;
};
#endif
