/*
 * ItemModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef ITEMMODEL_H_
#define ITEMMODEL_H_

#include "../Model.h"

class ItemModel : public Model {
public:
    ItemModel();
    ~ItemModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
};

#endif /* USERMODEL_H_ */
