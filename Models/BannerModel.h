/*
 * BannerModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef BANNERMODEL_H_
#define BANNERMODEL_H_

#include "../Model.h"

class BannerModel : public Model {
public:
    BannerModel();
    ~BannerModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
};

#endif /* USERMODEL_H_ */
