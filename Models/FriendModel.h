#ifndef _FRIENDMODEL_H_
#define _FRIENDMODEL_H_

#include <string>

class FriendModel {
public:
    void SetMyID(std::string id);
    std::string GetMyID();
    void SetFriendID(std::string id);
    std::string GetFriendID();

private:
    int id;
    std::string my_id;
    std::string friend_id;
};

#endif
