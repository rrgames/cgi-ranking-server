/*
 * GoodsModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "GoodsModel.h"

GoodsModel::GoodsModel() : Model() {
}
GoodsModel::~GoodsModel() {
}
const std::string GoodsModel::GetModelName() {
	return "good";
}
const std::string GoodsModel::GetTableName() {
    return "goods";
}
