/*
 * GoodsModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#ifndef GOODSMODEL_H_
#define GOODSMODEL_H_

#include "../Model.h"

class GoodsModel : public Model {
public:
    GoodsModel();
    ~GoodsModel();
    virtual	const std::string GetModelName();
    virtual	const std::string GetTableName();
};

#endif /* USERMODEL_H_ */
