/*
 * ItemModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "ItemModel.h"

ItemModel::ItemModel() : Model() {
}
ItemModel::~ItemModel() {
}
const std::string ItemModel::GetModelName() {
	return "item";
}
const std::string ItemModel::GetTableName() {
    return "items";
}
