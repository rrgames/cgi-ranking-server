/*
 * SessionModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "SessionModel.h"

SessionModel::SessionModel() {

}

SessionModel::SessionModel(std::string ip_addr,
                           std::string login_time,
                           std::string userID,
                           std::string access_token) {

    this->ip_addr = ip_addr;
    this->login_time = login_time;
    this->userID = userID;
    this->session_token = StringHelper::GetHash(ip_addr + login_time + userID);
    this->access_token = access_token;
}

std::string SessionModel::ToJsonString() {
    std::string json = "{\"session\":{";
    json +=
        "\"ip_addr\":\"" + ip_addr + "\"," +
        "\"login_time\":\"" + login_time + "\"," +
        "\"user_id\":\"" + userID + "\"," +
        "\"session_token\":\"" + session_token + "\""  +
        + "}}";
    return json;
}

std::string SessionModel::GetSessionToken() {
    return this->session_token;
}
std::string SessionModel::GetUserID() {
    return this->userID;
}
std::string SessionModel::GetFacebookToken() {
    return this->access_token;
}

