/*
 * UserModel.h
 *
 *  Created on: 2014. 9. 14.
 *      Author: into_arena
 */

#ifndef SESSIONMODEL_H_
#define SESSIONMODEL_H_
#include <string>
#include "../StringHelper.h"

class SessionModel {
public:
    SessionModel();
    SessionModel(std::string ip_addr,
                 std::string login_time,
                 std::string userID,
                 std::string access_token);

    std::string ToJsonString();
    std::string GetSessionToken();
    std::string GetUserID();
    std::string GetFacebookToken();

private:
    std::string ip_addr;
    std::string login_time;
    std::string userID;
    std::string session_token;
    std::string access_token;


};

#endif /* USERMODEL_H_ */
