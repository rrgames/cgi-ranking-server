/*
 * StageModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: into_arena
 */

#include "StageModel.h"

StageModel::StageModel() : Model() {

}
Model* StageModel::Create() {
    return new StageModel();
}
const std::string StageModel::GetModelName() {
    return "stage";
}
const std::string StageModel::GetTableName() {
    return "stages";
}
