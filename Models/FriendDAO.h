#ifndef FRIENDDAO_H
#define FRIENDDAO_H
#include <bson.h>
#include <mongoc.h>
#include <stdio.h>
#include <string>
#include <vector>
#include "FriendModel.h"

class FriendDAO {
public:
    FriendDAO();
    ~FriendDAO();
    void Insert(FriendModel fm);
    void Update(FriendModel fm);
    void Erase(FriendModel fm);
    void EraseByMyID(std::string myId);
    void Select();
    std::vector<FriendModel *> FindByUserID(std::string user);

private:
    std::string collectionName;
	std::string dbName;
    mongoc_database_t* database;
};


#endif
