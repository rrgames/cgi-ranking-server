/*
 * RecordModel.cpp
 *
 *  Created on: 2014. 9. 14.
 *      Author: bae-unidev
 */

#include "RecordModel.h"

RecordModel::RecordModel() : Model() {

}
Model* RecordModel::Create() {
    return new RecordModel();
}
const std::string RecordModel::GetModelName() {
    return "record";
}
const std::string RecordModel::GetTableName() {
    return "records";
}

