#include "FriendDAO.h"

#include "../Database/MongoDBPool.h"

FriendDAO::FriendDAO() {
    this->collectionName = "friends";
	this->dbName = "newdb_development";
}

FriendDAO::~FriendDAO() {

}

void FriendDAO::Insert(FriendModel fm) {
    //연결 얻어오기.
    mongoc_client_t* client = MongoDBPool::GetInstance()->GetClientFromPool();

    mongoc_collection_t* collection = mongoc_client_get_collection(client, dbName.c_str(), collectionName.c_str());

    bson_error_t error;
    bson_oid_t oid;
    bson_t *doc;

    // 객체 정의 시작.
    doc = bson_new();
    bson_oid_init(&oid, NULL);
    BSON_APPEND_OID(doc, "_id", &oid);
    BSON_APPEND_UTF8(doc, "my_id", fm.GetMyID().c_str());
    BSON_APPEND_UTF8(doc, "friend_id", fm.GetFriendID().c_str());
    // 객체 정의 종료.

    if(!mongoc_collection_insert(collection, MONGOC_INSERT_NONE, doc, NULL, &error)) {
        ///에러 처리함.
    }
    bson_destroy(doc);
    // 콜렉션 해제
    mongoc_collection_destroy(collection);
    // 도큐먼트 해제.
    //연결 해제하기.
    MongoDBPool::GetInstance()->ReleaseClientToPool(client);
}

void FriendDAO::Update(FriendModel fm) {

}

std::vector<FriendModel *> FriendDAO::FindByUserID(std::string user_id) {
    std::vector<FriendModel *> fm;

    mongoc_client_t* client = MongoDBPool::GetInstance()->GetClientFromPool();
    mongoc_collection_t* collection = mongoc_client_get_collection(client, dbName.c_str(), collectionName.c_str());

    mongoc_cursor_t* cursor;

    const bson_t* doc;
    bson_t* query;

    //char * str;
    query = bson_new();

    BSON_APPEND_UTF8(query, "my_id", user_id.c_str());

    cursor = mongoc_collection_find(collection, MONGOC_QUERY_NONE, 0, 0, 0, query, NULL, NULL);

    bson_iter_t iter;
    FriendModel* tempModel;
    while(mongoc_cursor_next(cursor, &doc)) {
        //str = bson_as_json (doc, NULL);

        ///sscanf(str, "%*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %*s %s", buffer);
        if(bson_iter_init_find(&iter, doc, "friend_id")) {
            tempModel = new FriendModel();
            tempModel->SetFriendID(bson_iter_utf8(&iter, NULL));
            tempModel->SetMyID(user_id);
            fm.push_back(tempModel);
        }
        //bson_free(str);
    }
    bson_destroy(query);

    mongoc_cursor_destroy(cursor);
    mongoc_collection_destroy(collection);
    MongoDBPool::GetInstance()->ReleaseClientToPool(client);
    return fm;
}

void FriendDAO::Erase(FriendModel fm) {

    mongoc_client_t* client = MongoDBPool::GetInstance()->GetClientFromPool();
    mongoc_collection_t* collection = mongoc_client_get_collection(client, dbName.c_str(), collectionName.c_str());

    bson_t* doc;
    bson_oid_t oid;
    bson_error_t error;

    doc = bson_new();
    BSON_APPEND_UTF8(doc, "my_id", fm.GetMyID().c_str());
    BSON_APPEND_UTF8(doc, "friend_id", fm.GetFriendID().c_str());

    if(!mongoc_collection_remove(collection, MONGOC_REMOVE_SINGLE_REMOVE, doc, NULL, &error)) {
        ///에러 처리한다.
    }

    mongoc_collection_destroy(collection);
    MongoDBPool::GetInstance()->ReleaseClientToPool(client);
}

void FriendDAO::EraseByMyID(std::string myId) {
    mongoc_client_t* client = MongoDBPool::GetInstance()->GetClientFromPool();
    mongoc_collection_t* collection = mongoc_client_get_collection(client,dbName.c_str(), collectionName.c_str());

    bson_t* doc;
    bson_oid_t oid;
    bson_error_t error;

    doc = bson_new();
    BSON_APPEND_UTF8(doc, "my_id", myId.c_str());

    // 이것은 위와 달리 조건에 맞는건 '전부 삭제함'
    if(!mongoc_collection_remove(collection, MONGOC_REMOVE_NONE, doc, NULL, &error)) {
        ///에러 처리한다.
    }
    mongoc_collection_destroy(collection);
    MongoDBPool::GetInstance()->ReleaseClientToPool(client);
}
