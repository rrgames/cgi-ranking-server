#include "SessionContainer.h"

SessionContainer * SessionContainer::instance = NULL;

SessionContainer::SessionContainer() {
}

SessionContainer::~SessionContainer() {
}

void SessionContainer::AppendSession(SessionModel* sm) {
    std::string key = "sessions:" + sm->GetSessionToken();
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
    conn->run(command("SET") << key  << sm->GetUserID());
    conn->run(command("SET") << key + ":FBToken" << sm->GetFacebookToken());
    conn->run(command("SET") << key + ":json" << sm->ToJsonString());

    conn->run(command("SET") << "users:" + sm->GetUserID() + ":session" << sm->GetSessionToken());

	// this is for expirement.
	conn->run(command("EXPIRE") << "users:" + sm->GetUserID() + ":session" << "3600");
    RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
}

int SessionContainer::GetUserID(std::string session) {
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
    int id = -1;
    if (conn->run(command("EXISTS") << "sessions:" + session).integer() == 1)
        id = atoi(conn->run(command("GET") << "sessions:" + session).str().c_str());
    RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
    return id;
}
std::string SessionContainer::GetSessionData(int userId) {
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
    std::string session = "";
    if (conn->run(command("EXISTS") << "users:" + StringHelper::IntToString(userId) + ":session").integer() == 1)
        session = conn->run(command("GET") << "sessions:" + conn->run(command("GET") << "users:" + StringHelper::IntToString(userId) + ":session").str() + ":json").str();
    RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
    return session;
}

std::string SessionContainer::GetFacebookTokenFindBySession(std::string session) {
    connection::ptr_t conn = RedisPool::GetInstance()->GetConnectionFromPool();
    std::string fbToken = "";
    if (conn->run(command("EXISTS") << "sessions:" + session).str() == "1")
        fbToken = conn->run(command("GET") << "sessions:" + session + ":FBToken").str();
    RedisPool::GetInstance()->ReleaseConnectionToPool(conn);
    return fbToken;
}
