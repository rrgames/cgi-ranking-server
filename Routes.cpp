/*
 * Routes.cpp
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#include "Routes.h"
#include <cstdlib>
#include <string.h>
#include "StringHelper.h"
#include "GAModule.h"
#include "Environment.h"
Routes::Routes() {
    //여기서 기본 루트에 대한 정보를 삽입하자.
}

void Routes::RouteUrl() {
    Request request;

    if (request.HasError()) {
        Response* response = Response::CreateResponse(400, "{ error_message : 'request is error' }");
        response->Render();
        Response::RemoveResponse(response);
        return;
    }

    std::string controllerName = setRoute(request);
    if(controllerName == "") {
        Response* response = Response::CreateResponse(400, "hell-o?");
        response->Render();
        Response::RemoveResponse(response);
        return;
    }

    Controller * controller = Controller::CreateController(controllerName);
    controller->SetRequest(&request);

	/// this is for GA Module//
//	GAModule ga;
//	ga.Commit(Environment::GetEnv(Environment::RequestUri), Environment::GetEnv(Environment::RemoteAddr));
	//GA Module Finished.
    Response* response = controller->Action();
    response->Render();
    Response::RemoveResponse(response);

    Controller::RemoveController(controller);

}

const std::string Routes::setRoute(Request& request) {

//    std::vector<std::string> paths;
//	StringHelper::SplitByToken("/?", Environment::GetEnv(Environment::RequestUri), &paths);

  //  int pathlen = paths.size();
    //paths[pathlen-1] = paths[pathlen-1].substr(0, paths[pathlen-1].find("?"));
	std::string mt1= request.GetParam("method_1");
	std::string mt2= request.GetParam("method_2");
	if(mt1 == "stages") {
		if(mt2 == "records")
			return "records";//return records;
		return "stages"; // return stages;
	} else if(mt1 == "users") {
		if(mt2 == "friends")
			// 혹은 추후에 아이템인 경우에도 여기서 리턴한다.
			return "friends";
		else if(mt2 == "items")
			return "items";
		else if(mt2 == "replays")
			return "replay";
		return "users";
	} else if(mt1 == "session") {
		return "session";
	} else if(mt1 == "market") {
		return "market";
	}
	return "";
}
