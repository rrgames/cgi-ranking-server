/*
 * Controller.h
 *
 *  Created on: 2014. 9. 6.
 *      Author: bae-unidev
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include <string>

#include "Response.h"
#include "Request.h"
#include "StringHelper.h"
#include "JsonParser/JsonParser.h"

class Controller {
public:
    void SetRequest(Request* request);
    virtual Response* Action();
    virtual	const std::string GetControllerName() = 0;
    virtual const std::string GetModelName() = 0;

    static Controller* CreateController(std::string controllerName);
    static void RemoveController(Controller* pointer);

protected:
    virtual Response* ActionIndex() = 0;
    virtual Response* ActionShow() = 0;
    virtual Response* ActionCreate() = 0;
    virtual Response* ActionUpdate() = 0;
    virtual Response* ActionDelete() = 0;

    Request* request;
};

#endif /* CONTROLLER_H_ */
